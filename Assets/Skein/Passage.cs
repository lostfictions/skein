using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using UniRx;
using UnityEngine.Assertions;

namespace Skein
{
    [Serializable]
    public class Passage : IDisposable
    {
        public StringReactiveProperty Title { get; private set; }
        public StringReactiveProperty Text { get; private set; }
        public Vector2ReactiveProperty Position { get; private set; }
        public TextStyleReactiveProperty PassageTextStyle { get; private set; }
        public TextStyleReactiveProperty PassageLinkStyle { get; private set; }
        public EventSheet EventSheet { get; private set; }
        public StyleMap StyleMap { get; private set; }

        [FullSerializer.fsIgnore]
        public ReadOnlyReactiveProperty<INode> ParsedText { get; private set; }

        public Passage(string initialName, Vector2 initialPosition, TextStyle initialTextStyle = null, TextStyle initialLinkStyle = null)
        {
            Title = new StringReactiveProperty(initialName);
            Text = new StringReactiveProperty("Enter text here");
            Position = new Vector2ReactiveProperty(initialPosition);
            PassageTextStyle = initialTextStyle == null ?
                new TextStyleReactiveProperty() :
                new TextStyleReactiveProperty(initialTextStyle);
            PassageLinkStyle = initialLinkStyle == null ?
                new TextStyleReactiveProperty() :
                new TextStyleReactiveProperty(initialLinkStyle);
            EventSheet = new EventSheet();
            StyleMap = new StyleMap();

            ConnectParsedText();
        }

        public void ConnectParsedText()
        {
            Assert.IsNull(ParsedText);

            ParsedText = Text
                .DistinctUntilChanged()
                .Select(t => Parser.Parse(t))
                .ToReadOnlyReactiveProperty();
        }

        public void Dispose()
        {
            Title.Dispose();
            Text.Dispose();
            Position.Dispose();
            PassageTextStyle.Dispose();
            PassageLinkStyle.Dispose();
            EventSheet.Dispose();
            StyleMap.Dispose();
            ParsedText.Dispose();
        }
    }

    #region UniRx boilerplate
    [Serializable] public class EventSheet : ReactiveCollection<Event> {}
    [Serializable] public class StyleMap : ReactiveDictionary<string, TextStyle> {}
#if UNITY_EDITOR
    [UnityEditor.CustomPropertyDrawer(typeof(EventSheet))]
    [UnityEditor.CustomPropertyDrawer(typeof(StyleMap))]
    // ReSharper disable RedundantExtendsListEntry
    public partial class ExtendInspectorDisplayDrawer : InspectorDisplayDrawer { }
    // ReSharper restore RedundantExtendsListEntry
#endif
    #endregion
}
