//#define NOFASTCALL

#if !NOFASTCALL && (UNITY_EDITOR || UNITY_STANDALONE)
#define CANFASTCALL
using Vexe.Runtime.Extensions;
#endif

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UniRx;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Skein
{
    #region ACE info

    public class WeaverInfo
    {
        public Action[] actions;
        public Condition[] conditions;
        public Expression[] expressions;

        static readonly Dictionary<Type, WeaverInfo> infoByType;

        static WeaverInfo()
        {
            infoByType = Assembly.GetAssembly(typeof(Story))
                .GetTypes()
                .Select(type => new {
                    type,
                    actions = type.GetMethods().Where(mi => Attribute.IsDefined(mi, typeof(ActionAttribute))),
                    conditions = type.GetMethods().Where(mi => Attribute.IsDefined(mi, typeof(ConditionAttribute))),
                    expressions = type.GetMethods().Where(mi => Attribute.IsDefined(mi, typeof(ExpressionAttribute)))
                })
                .Where(info => info.actions.Any() || info.conditions.Any() || info.expressions.Any())
                .ToDictionary(info => info.type, info => new WeaverInfo {
                    actions = info.actions.Select(mi => Action.MakeFromMethodInfo(mi)).ToArray(),
                    conditions = info.conditions.Select(mi => Condition.MakeFromMethodInfo(mi)).ToArray(),
                    expressions = info.expressions.Select(mi => Expression.MakeFromMethodInfo(mi)).ToArray(),
                });
        }

        public static WeaverInfo Get(Type objectType)
        {
            Assert.IsTrue(typeof(Object).IsAssignableFrom(objectType));
            return infoByType[objectType];
        }

        public IEnumerable<T> GetAll<T>() where T : IAceInfo
        {
            var tt = typeof(T);
            if(tt == typeof(Action)) {
                return actions.Cast<T>();
            }
            if(tt == typeof(Condition)) {
                return conditions.Cast<T>();
            }
            //tt == typeof(Expression)
            return expressions.Cast<T>();
        }
    }

    public class Action : AceInfo<ActionAttribute, object>
    {
        internal static Action MakeFromMethodInfo(MethodInfo mi)
        {
            return MakeFromMethodInfo<Action>(mi);
        }
    }

    public class Condition : AceInfo<ConditionAttribute, bool>
    {
        internal static Condition MakeFromMethodInfo(MethodInfo mi)
        {
            return MakeFromMethodInfo<Condition>(mi);
        }
    }

    public class Expression : AceInfo<ExpressionAttribute, object>
    {
        static readonly ArgumentInfo[] emptyArgs = new ArgumentInfo[0];
        public override ArgumentInfo[] Arguments { get { return emptyArgs; } }
        public Type ReturnType { get; private set; }

        internal static Expression MakeFromMethodInfo(MethodInfo mi)
        {
            var e = MakeFromMethodInfo<Expression>(mi);
            e.ReturnType = mi.ReturnType;
            return e;
        }
    }

    public abstract class AceInfo<TAttribute, TReturn> : IAceInfo
        where TAttribute : MethodDescriptionAttribute
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public virtual ArgumentInfo[] Arguments { get; private set; }
        public string MethodId { get; private set; }
#if CANFASTCALL
        public MethodCaller<Object, TReturn> MethodCaller { get; private set; }
#else
        public Func<Object, object[], TReturn> MethodCaller { get; private set; }
#endif

        protected static TAceInfo MakeFromMethodInfo<TAceInfo>(MethodInfo mi)
             where TAceInfo : AceInfo<TAttribute, TReturn>, new()
        {
            var a = (TAttribute)mi.GetCustomAttributes(typeof(TAttribute), false).First();
            var parameters = mi.GetParameters();
            Assert.AreEqual(a.ArgumentDescriptions.Length, parameters.Length);

            var methodId = GetMethodId(mi);
            var info = new TAceInfo {
                Name = a.Name,
                Description = a.Description,
                Arguments = a.ArgumentDescriptions
                    .Select((desc, i) =>
                        new ArgumentInfo { description = desc, type = parameters[i].ParameterType })
                    .ToArray(),
                MethodId = methodId,
#if CANFASTCALL
                MethodCaller = mi.DelegateForCall<Object, TReturn>()
#else
                MethodCaller = (caller, args) => (TReturn)mi.Invoke(caller, args)
#endif
            };

            InfoStorage<TAceInfo>.Add(methodId, info);

            return info;
        }

        static string GetMethodId(MethodInfo mi)
        {
            return mi.ReflectedType + ":" +
                   mi.Name + ":" +
                   mi.GetParameters().Select(pi => pi.ParameterType).Log(",");
        }
    }

    public static class InfoStorage<TAceInfo> where TAceInfo : IAceInfo
    {
        static readonly Dictionary<string, TAceInfo> infoById = new Dictionary<string, TAceInfo>();

        internal static void Add(string methodId, TAceInfo info)
        {
            infoById.Add(methodId, info);
        }

        public static TAceInfo GetFromMethodId(string methodId)
        {
            return infoById[methodId];
        }
    }

    public interface IAceInfo
    {
        string Name { get; }
        string Description { get; }
        ArgumentInfo[] Arguments { get; }
        string MethodId { get; }
    }

    public class ArgumentInfo
    {
        public string description;
        public Type type;
    }

    #endregion


    #region Serializable Event Data

    /// An item in a Weaver event sheet.
    [Serializable]
    public class Event : IDisposable
    {
        public ReactiveCollection<ConditionData> conditions = new ReactiveCollection<ConditionData>();
        public ReactiveCollection<ActionData> actions = new ReactiveCollection<ActionData>();
        public void Dispose()
        {
            conditions.Dispose();
            actions.Dispose();
        }
    }

    [Serializable]
    [FullSerializer.fsObject(Converter = typeof(ActionDataConverter))]
    public class ActionData : AceData<Action>
    {
        public void Invoke()
        {
            info.MethodCaller(caller, args);
        }
    }

    [Serializable]
    [FullSerializer.fsObject(Converter = typeof(ConditionDataConverter))]
    public class ConditionData : AceData<Condition>
    {
        public bool Test()
        {
            return info.MethodCaller(caller, args);
        }
    }

    public abstract class AceData<T> where T : IAceInfo
    {
        public T info;
        public Object caller;
        public object[] args;
    }

    public static class AceDataHelper
    {
        public static T Clone<T, U>(this T original) where T : AceData<U>, new() where U : IAceInfo
        {
            return new T {
                info = original.info,
                caller = original.caller,
                args = (object[])original.args.Clone()
            };
        } 
    }

    #endregion

    #region Attributes

    [Serializable]
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class ActionAttribute : MethodDescriptionAttribute
    {
        public ActionAttribute(string name, string description, params string[] argumentDescriptions)
            : base(name, description, argumentDescriptions) {}
    }

    [Serializable]
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class ConditionAttribute : MethodDescriptionAttribute
    {
        public ConditionAttribute(string name, string description, params string[] argumentDescriptions)
            : base(name, description, argumentDescriptions) {}
    }

    [Serializable]
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class ExpressionAttribute : MethodDescriptionAttribute
    {
        public ExpressionAttribute(string name, string description)
            : base(name, description, null) { }
    }

    [Serializable]
    public abstract class MethodDescriptionAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string[] ArgumentDescriptions { get; private set; }

        protected MethodDescriptionAttribute(string name, string description, params string[] argumentDescriptions)
        {
            Name = name;
            Description = description;
            ArgumentDescriptions = argumentDescriptions;
        }        
    }

    #endregion
}
