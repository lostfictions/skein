using System;
using UnityEngine;

namespace Skein
{
    [Serializable]
    public class Font : ScriptableObject
    {

    }

    [Serializable]
    public class Glyph
    {
        public Rect rect;
	    public float xOffset;
	    public float yOffset;
	    public float xAdvance;
        public CharsToKerning kerning;

	    public float GetKerning(char previousChar)
	    {
		    float k = 0f;

		    if(kerning == null) {
		        return k;
		    }

		    kerning.TryGetValue(previousChar, out k);
		    return k;
	    }
    }

    [Serializable] public class CharsToKerning : SerializableDictionary<char, float> {};
}
