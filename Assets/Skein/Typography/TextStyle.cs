using UnityEngine;
using System;
using UniRx;

namespace Skein
{
    public enum TextAlignment
    {
        Left,
        Center,
        Right
    }

    [Serializable]
    public class TextStyle
    {
        public Material fontMaterial;
        public TypogenicFont font;
        public float size = 16f;
        public float leading = 0f;
        public float tracking = 0f;
        public float paragraphSpacing = 0f;
        public TextAlignment alignment = TextAlignment.Left;

        // HACK: since Unity doesn't have UI for interacting with nullable types
        // in the inspector, we define a bool property for each value type declaring
        // whether it should be used. Yeah, it's silly.

        // When we stop using the Unity inspector/serialization we could maybe
        // switch these over to nullable types for clarity.
        public bool useSize;
        public bool useLeading;
        public bool useTracking;
        public bool useParagraphSpacing;
        public bool useAlignment;
    }

    #region UniRx Boilerplate
    [Serializable]
    public class TextStyleReactiveProperty : ReactiveProperty<TextStyle>
    {
        public TextStyleReactiveProperty() { }
        public TextStyleReactiveProperty(TextStyle textStyle) : base(textStyle) { }

    }
#if UNITY_EDITOR
    [UnityEditor.CustomPropertyDrawer(typeof(TextStyleReactiveProperty))]
    public partial class ExtendInspectorDisplayDrawer : InspectorDisplayDrawer { }
#endif
    #endregion
}
