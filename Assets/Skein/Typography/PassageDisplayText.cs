using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine.Assertions;
using UnityEngine.Assertions.Must;
using UniRx;
using UniRx.Triggers;

namespace Skein
{
    using VertsUvsTris = Tuple<List<Vector3>, List<Vector2>, List<List<int>>>;

    [RequireComponent(typeof(RectTransform))]
    public class PassageDisplayText : MonoBehaviour
    {
        RectTransform rt;
        MeshRenderer mr;
        Mesh mesh;
        readonly List<GameObject> childTriggers = new List<GameObject>();

        readonly HashSet<string> clickedTriggers = new HashSet<string>();
        readonly HashSet<string> hoveredTriggers = new HashSet<string>();

        void Awake()
        {
            rt = GetComponent<RectTransform>();
            rt.MustNotBeNull();

            var mf = gameObject.AddOrGetComponent<MeshFilter>();
            if(mf.sharedMesh == null) {
                mf.sharedMesh = new Mesh();
            }
            mesh = mf.sharedMesh;
            mr = gameObject.AddOrGetComponent<MeshRenderer>(); 
        }

        void LateUpdate()
        {
            clickedTriggers.Clear();
            hoveredTriggers.Clear();
        }

        public void RebuildMesh(Passage passage, TextStyle defaultTextStyle, TextStyle defaultLinkStyle)
        {
            mesh.Clear();
            foreach(var go in childTriggers) {
                Destroy(go);
            }
            childTriggers.Clear();

            if(passage == null || string.IsNullOrEmpty(passage.Text.Value.Trim())) {
                return;
            }

            string rootName = ((GroupNode)passage.ParsedText.Value).name;

            var vertsUvsTris = Tuple.Create(new List<Vector3>(), new List<Vector2>(), new List<List<int>>());

            // To determine the default style to render this passage with, we use the
            // passage's default style if it exists, or the global default style.
            var passageTextStyle = passage.PassageTextStyle.Value ?? defaultTextStyle;
            var passageLinkStyle = passage.PassageLinkStyle.Value ?? defaultLinkStyle;

            var lines = GetWrappedText(passage, passageTextStyle, passageLinkStyle, rt.sizeDelta.x);

            float yPos = 0f;
            TextAlignment? paragraphAlignment = null;
            var mats = new Dictionary<Material, int>();
            var triggers = new Dictionary<string, GameObject>();
            foreach(var line in lines) {
                if(line.styledChunks.Count == 1 && line.styledChunks[0].text == "\n") {
                    // If a line has a single chunk containing only a newline, it's a special case
                    // resulting from a manual line feed. All we do is add paragraph spacing.

                    // Yes, "\n" kind of is a magic value, sorry. GetWrappedText should strip it in all other cases.

                    yPos += line.styledChunks[0].style.paragraphSpacing;

                    //We also reset the paragraph alignment, so the next chunk will get a new one.
                    paragraphAlignment = null;
                }
                else {
                    // For alignment, we use the first chunk's in a paragraph's alignment, or
                    // the default style's alignment if the first chunk doesn't have an alignment defined.
                    if(!paragraphAlignment.HasValue) {
                        var firstStyle = line.styledChunks[0].style;
                        paragraphAlignment = (firstStyle != null && firstStyle.useAlignment) ?
                            firstStyle.alignment :
                            passageTextStyle.alignment;
                    }

                    float xPos;
                    switch(paragraphAlignment) {
                        case TextAlignment.Right:
                            xPos = rt.sizeDelta.x - line.lineWidth;
                            break;
                        case TextAlignment.Center:
                            xPos = (rt.sizeDelta.x - line.lineWidth) / 2f;
                            break;
                        default:
                            xPos = 0f;
                            break;
                    }

                    foreach(var styledChunk in line.styledChunks) {
                        int matIndex;
                        if(!mats.TryGetValue(styledChunk.style.fontMaterial, out matIndex)) {
                            matIndex = mats.Count;
                            mats.Add(styledChunk.style.fontMaterial, matIndex);
                        }

                        GameObject triggerGo = null;
                        string groupName = styledChunk.name;
                        if(groupName != rootName && !triggers.TryGetValue(groupName, out triggerGo)) {
                            triggerGo = new GameObject(groupName);
                            var t = triggerGo.transform;
                            t.parent = transform;
                            t.localPosition = Vector3.zero;
                            t.localScale = Vector3.one;
                            t.localRotation = Quaternion.identity;
                            triggerGo
                                .OnMouseUpAsButtonAsObservable()
                                .Subscribe(_ => clickedTriggers.Add(groupName))
                                .AddTo(triggerGo);

                            triggerGo
                                .OnMouseEnterAsObservable()
                                .Subscribe(_ => hoveredTriggers.Add(groupName))
                                .AddTo(triggerGo);

                            triggers.Add(groupName, triggerGo);
                            childTriggers.Add(triggerGo);
                        }

                        xPos = BlitChunk(styledChunk, xPos, yPos, vertsUvsTris, matIndex, triggerGo);
                    }
                }

                var lineStyles = line.styledChunks.Select(sc => sc.style).ToArray();   
                yPos += lineStyles.Select(s => s.font.LineHeight).Max() *
                    lineStyles.Select(s => s.size).Max() +
                    lineStyles.Select(s => s.leading).Max();
            }

            mesh.subMeshCount = mats.Count;
            mesh.SetVertices(vertsUvsTris.Item1);
            mesh.SetUVs(0, vertsUvsTris.Item2);
            vertsUvsTris.Item3.Each((triList, i) => mesh.SetTriangles(triList.ToArray(), i));
            mr.sharedMaterials = mats.OrderBy(kvp => kvp.Value).Select(kvp => kvp.Key).ToArray();
        }

        #region Actions, Conditions, Expressions

        [Condition("Group is clicked", "Triggers when a group of given name is clicked or tapped", "Name of the group to check")]
        public bool WasClicked(string groupName)
        {
            return clickedTriggers.Contains(groupName);
        }

        [Condition("Group is hovered over", "Triggers when a group of given name is moused over", "Name of the group to check")]
        public bool WasHovered(string groupName)
        {
            return hoveredTriggers.Contains(groupName);
        }

        #endregion

        #region Blitting

        static float BlitChunk(StyledChunk styledChunk, float xPos, float yPos, VertsUvsTris vertsUvsTris, int matIndex, GameObject triggerGo)
        {
            var style = styledChunk.style;

            TGlyph prevGlyph = null;
            foreach(char c in styledChunk.text) {

                var glyph = style.font.Glyphs.Get(c);

                if(glyph == null) {
                    Debug.LogWarning("Glyph not found in character set: \"" + c + "\"");
                    continue;
                }

                float kerning = GetKerning(prevGlyph, c, style.size);

                var localSpaceQuad = new Rect(
                    xPos + glyph.xOffset * style.size + kerning,
                    yPos + glyph.yOffset * style.size,
                    glyph.rect.width * style.size,
                    glyph.rect.height * style.size);

                if(triggerGo != null) {
                    var bc = triggerGo.AddComponent<BoxCollider>();
                    bc.isTrigger = true;
                    bc.center = new Vector3(
                        localSpaceQuad.x + localSpaceQuad.width / 2f,
                        -(localSpaceQuad.y + localSpaceQuad.height / 2f),
                        0);
                    bc.size = new Vector3(localSpaceQuad.width, localSpaceQuad.height, 0.01f);
                }

                if(!char.IsWhiteSpace(c)) {
                    BlitQuad(localSpaceQuad, glyph.rect, style.font.HScale, style.font.VScale, vertsUvsTris, matIndex);
                }

                xPos += glyph.xAdvance * style.size + style.tracking + kerning;
                prevGlyph = glyph;
            }
            return xPos;
        }

        static void BlitQuad(Rect localSpaceQuad, Rect uvQuad, float hScale, float vScale, VertsUvsTris vertsUvsTris, int matIndex)
        {
            var verts = vertsUvsTris.Item1;
            var uvs = vertsUvsTris.Item2;
            var tris = vertsUvsTris.Item3;

            int trisStartIndex = verts.Count;

            verts.Add(new Vector3(localSpaceQuad.x, -localSpaceQuad.y, 0f));
            verts.Add(new Vector3(localSpaceQuad.x + localSpaceQuad.width, -localSpaceQuad.y, 0f));
            verts.Add(new Vector3(localSpaceQuad.x + localSpaceQuad.width, -localSpaceQuad.y - localSpaceQuad.height, 0f));
            verts.Add(new Vector3(localSpaceQuad.x, -localSpaceQuad.y - localSpaceQuad.height, 0f));

            uvs.Add(new Vector2(uvQuad.x / hScale, 1 - uvQuad.y / vScale));
            uvs.Add(new Vector2((uvQuad.x + uvQuad.width) / hScale, 1 - uvQuad.y / vScale));
            uvs.Add(new Vector2((uvQuad.x + uvQuad.width) / hScale, 1 - (uvQuad.y + uvQuad.height) / vScale));
            uvs.Add(new Vector2(uvQuad.x / hScale, 1 - (uvQuad.y + uvQuad.height) / vScale));

            while(tris.Count < matIndex + 1) {
                tris.Add(new List<int>());
            }

            tris[matIndex].Add(trisStartIndex);
            tris[matIndex].Add(trisStartIndex + 1);
            tris[matIndex].Add(trisStartIndex + 2);
            tris[matIndex].Add(trisStartIndex);
            tris[matIndex].Add(trisStartIndex + 2);
            tris[matIndex].Add(trisStartIndex + 3);
        }

        #endregion

        #region Wrapping

        [Pure]
        static List<StyledLine> GetWrappedText(Passage passage, TextStyle passageTextStyle, TextStyle passageLinkStyle, float rectWidth)
        {
            var root = (GroupNode)passage.ParsedText.Value;

            // We iterate through the root's children once manually to assign them the default style
            // if they're text nodes, then pass in the link style for all other groups.
            // TODO: not a sustainable solution once we need to distinguish between clickable and non-clickable groups :(

            var chunkedText = new List<StyledChunk>();
            foreach(var cn in root.children) {
                var tn = cn as TextNode;
                if(tn != null) {
                    chunkedText.Add(new StyledChunk {name = root.name, text = tn.text, style = passageTextStyle});
                }
                else {
                    var gn = (GroupNode)cn;
                    TextStyle mergedStyle;
                    TextStyle lookupStyle;
                    if(passage.StyleMap.TryGetValue(gn.name, out lookupStyle)) {
                        mergedStyle = MergeStyle(passageLinkStyle, lookupStyle);
                    }
                    else {
                        mergedStyle = passageLinkStyle;
                    }

                    // Now recurse, passing the merged style and group name to our children.
                    foreach(var c in gn.children) {
                        ChunkText(c, passage.StyleMap, mergedStyle, gn.name, ref chunkedText);
                    }
                }
            }

//            ChunkText(passage.ParsedText.Value, passage.StyleMap, passageTextStyle, null, ref chunkedText);
//            Debug.Log("Chunking result: " + chunkedText.Select(c => c.name + "->" + c.text).Log("~"));
            return GetWrappedLines(chunkedText, rectWidth);
        }

        static void ChunkText(INode node, StyleMap styles, TextStyle style, string name, ref List<StyledChunk> chunks)
        {
            // If we're a text node, return a StyledChunk of the text plus the style we were passed.
            var tn = node as TextNode;
            if(tn != null) {
                chunks.Add(new StyledChunk{ name = name, text = tn.text, style = style });
                return;
            }

            // Otherwise, we're a group node. If there's a style assigned to us, merge it with our
            // parent, overriding any of the properties we've defined.
            var gn = (GroupNode)node;
            TextStyle mergedStyle;
            TextStyle lookupStyle;
            if(styles.TryGetValue(gn.name, out lookupStyle)) {
                mergedStyle = MergeStyle(style, lookupStyle);
            }
            else {
                mergedStyle = style;
            }

            // Now recurse, passing the merged style and group name to our children.
            foreach(var c in gn.children) {
                ChunkText(c, styles, mergedStyle, gn.name, ref chunks);
            }
        }

        ///Given an enumerable of styled chunks, return a list of one or more wrapped, styled lines.
        [Pure]
        static List<StyledLine> GetWrappedLines(IEnumerable<StyledChunk> styledChunks, float rectWidth)
        {
            var wrappedLines = new List<StyledLine>();

            var line = new StyledLine();
            foreach(var chunk in styledChunks) {
                // Normalize all CRLFs to LFs to avoid cross-platform weirdness, and split on them.
                var lineBreakSubChunks = chunk.text.Replace("\r\n", "\n").Split('\n');

                // Iterate through all the broken lines except the last one.
                // (That means we'll skip this block if there's only one line!)
                foreach(var subChunk in lineBreakSubChunks.Take(lineBreakSubChunks.Length - 1)) {
                    AppendStyledChunk(subChunk, chunk, ref line, ref wrappedLines, rectWidth);

                    // At every manual line feed, append the current line...
                    if(line.styledChunks.Count > 0) {
                        wrappedLines.Add(line);
                    }
                    line = new StyledLine();

                    // ...and insert an special StyledLine containing only a newline character, which we'll parse specially later.
                    wrappedLines.Add(new StyledLine { styledChunks = new List<StyledChunk> { new StyledChunk { name = chunk.name, text = "\n", style = chunk.style } } });
                }
                // Now insert the last chunk.
                AppendStyledChunk(lineBreakSubChunks[lineBreakSubChunks.Length - 1], chunk, ref line, ref wrappedLines, rectWidth);
            }

            if(line.styledChunks.Count > 0) {
                wrappedLines.Add(line);
            }

            //The last chunk and a few other line-ending chunks may still have trailing whitespace.
            foreach(var wl in wrappedLines) {
                var lastChunk = wl.styledChunks.Last();
                if(lastChunk.text.EndsWith(" ")) {
                    string trimmed = lastChunk.text.TrimEnd(' ');

                    float oldWidth = GetStringWidth(lastChunk.text, lastChunk.style);
                    if(string.IsNullOrEmpty(trimmed)) {
                        wl.styledChunks.RemoveAt(wl.styledChunks.Count - 1);
                        wl.lineWidth -= oldWidth;
                    }
                    else {
                        float newWidth = GetStringWidth(trimmed, lastChunk.style);
                        wl.lineWidth += newWidth - oldWidth;
                        wl.styledChunks[wl.styledChunks.Count - 1].text = trimmed;
                    }
                }
            }

            Assert.IsTrue(!wrappedLines.Any(l => l.styledChunks.First().text.StartsWith(" ")));
            Assert.IsTrue(!wrappedLines.Any(l => l.styledChunks.Last().text.EndsWith(" ")));

            return wrappedLines;
        }

        static void AppendStyledChunk(string text, StyledChunk chunk, ref StyledLine currentLine, ref List<StyledLine> wrappedLines, float rectWidth)
        {
            var style = chunk.style;
            var name = chunk.name;

            var toAdd = new StringBuilder();
            float runningWidth = currentLine.lineWidth;

            var subChunks = GetWrappableSubChunks(text);

            //If this is the first chunk of the first line, add it right away (since we should never wrap the first line.)
            int startIndex = 0;
            if(wrappedLines.Count == 0 && currentLine.styledChunks.Count == 0) {
                var initialChunk = subChunks.FirstOrDefault();
                if(!string.IsNullOrEmpty(initialChunk)) {
                    toAdd.Append(initialChunk);
                    runningWidth += GetStringWidth(initialChunk, style);
                }
                startIndex = 1;
            }

            for(int i = startIndex; i < subChunks.Count; i++) {
                float wordWidth = GetStringWidth(subChunks[i], style);

                if(runningWidth + wordWidth > rectWidth) {
                    //Since we're the last chunk of the line, trim the trailing whitespace.
                    //If we're also the first chunk of the line, trim leading whitespace too.
                    string adjustedToAdd = currentLine.styledChunks.Count == 0 ?
                        toAdd.ToString().Trim(' ') :
                        toAdd.ToString().TrimEnd(' ');

                    currentLine.styledChunks.Add(new StyledChunk { name = name, text = adjustedToAdd, style = style });

                    //Since we may have adjusted the string width by trimming, get the new final width.
                    currentLine.lineWidth += GetStringWidth(adjustedToAdd, style);

                    //Finally, add our line to wrappedLines and reset everything we're keeping track of.
                    wrappedLines.Add(currentLine);

                    currentLine = new StyledLine();
                    toAdd = new StringBuilder();
                    runningWidth = 0;
                }

                toAdd.Append(subChunks[i]);
                runningWidth += wordWidth;
            }

            if(toAdd.Length > 0) {
                //If we're the first chunk of the line, trim leading whitespace.
                string adjustedToAdd = currentLine.styledChunks.Count == 0 ?
                        toAdd.ToString().TrimStart(' ') :
                        toAdd.ToString();

                currentLine.styledChunks.Add(new StyledChunk { name = name, text = adjustedToAdd, style = style });

                //Since we may have adjusted the string width by trimming, get the adjusted width.
                currentLine.lineWidth += GetStringWidth(adjustedToAdd, style);
            }
        }

        static readonly char[] wrappingDelimiters = {' ', '-'};

        /// Given a string, get the string chopped up by the delimiters defined above in wrappingDelimiters,
        /// including the delimiters at the end of each segment. Thus "Thou art a snaggle-tooth" becomes
        /// ["Thou ", "art ", "a ", "snaggle-", "tooth"].
        [Pure]
        static List<string> GetWrappableSubChunks(string s)
        {
            var delimiterIndices = new List<int>();

            //We always at at least one index, at -1. This ensures we iterate
            //through the string in the next step starting with 0.
            int delimiterIndex = -1;
            do {
                delimiterIndices.Add(delimiterIndex);
                delimiterIndex = s.IndexOfAny(wrappingDelimiters, delimiterIndex + 1);
            } while(delimiterIndex != -1);

            var subChunks = new List<string>();
            for(int i = 0; i < delimiterIndices.Count - 1; i++) {
                subChunks.Add(s.Substring(delimiterIndices[i] + 1, delimiterIndices[i + 1] - delimiterIndices[i]));
            }
            int lastDelimInd = delimiterIndices[delimiterIndices.Count - 1];
            if(lastDelimInd != s.Length - 1) {
                subChunks.Add(s.Substr(lastDelimInd + 1, s.Length));
            }
            return subChunks;
        }

        #endregion

        #region Helpers

        static TextStyle MergeStyle(TextStyle parent, TextStyle child)
        {
            return new TextStyle {
                fontMaterial = child.fontMaterial ?? parent.fontMaterial,
                font = child.font ?? parent.font,
                size = child.useSize ? child.size : parent.size,
                leading = child.useLeading ? child.leading : parent.leading,
                tracking = child.useTracking ? child.tracking : parent.tracking,
                paragraphSpacing=  child.useParagraphSpacing ? child.paragraphSpacing : parent.paragraphSpacing,
                alignment = child.useAlignment ? child.alignment : parent.alignment,
            };
        }

        static float GetStringWidth(string s, TextStyle style)
        {
            TGlyph prevGlyph = null;
            float width = 0f;

            foreach(char c in s) {
                var glyph = style.font.Glyphs.Get(c);
                if(glyph == null) {
                    continue;
                }
                var kerning = GetKerning(prevGlyph, c, style.size);
                width += glyph.xAdvance * style.size + style.tracking + kerning;
                prevGlyph = glyph;
            }

            return width;
        }

        static float GetKerning(TGlyph prevGlyph, char c, float size)
        {
            return prevGlyph == null ? 0f : prevGlyph.GetKerning(c) * size;
        }

        #endregion

        class StyledLine
        {
            public List<StyledChunk> styledChunks = new List<StyledChunk>();
            public float lineWidth;
        }

        class StyledChunk
        {
            public string name;
            public string text;
            public TextStyle style;
        }
    }
}
