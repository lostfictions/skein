using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using UnityEngine.Assertions.Must;
using UniRx;
using UniRx.Triggers;

namespace Skein
{
    using VertsAndUvs = Tuple<List<Vector3>, List<Vector2>>;

    [RequireComponent(typeof(RectTransform))]
    public class GenericText : MonoBehaviour
    {
        public StringReactiveProperty text;
        public TextStyleReactiveProperty textStyle; 

        RectTransform rt;
        MeshRenderer mr;
        Mesh mesh;

        void Awake()
        {
            rt = GetComponent<RectTransform>();
            rt.MustNotBeNull();

            var mf = gameObject.AddOrGetComponent<MeshFilter>();
            if(mf.sharedMesh == null) {
                mf.sharedMesh = new Mesh();
            }
            mesh = mf.sharedMesh;
            mr = gameObject.AddOrGetComponent<MeshRenderer>(); 
        }

        void Start()
        {
            this.OnRectTransformDimensionsChangeAsObservable()
                .StartWith(new Unit()) // Force it to produce an initial observable
                .CombineLatest(text, (_, nextText) => nextText)
                .CombineLatest(textStyle, (nextText, nextStyle) => new { nextText, nextStyle })
                .Subscribe(next => RebuildMesh(next.nextText, next.nextStyle));
        }

        public void RebuildMesh(string nextText, TextStyle nextStyle)
        {
            mesh.Clear();
            if(string.IsNullOrEmpty(nextText.Trim())) {
                return;
            }

            var vertsAndUvs = Tuple.Create(new List<Vector3>(), new List<Vector2>());

            var lines = GetWrappedText(nextText, nextStyle, rt.sizeDelta.x);

            float yPos = 0f;
            foreach(var line in lines) {
                if(string.IsNullOrEmpty(line)) {
                    // If this is a blank line, treat it as a paragraph break and add the paragraph spacing.
                    // Note that consecutive lines will each apply paragraph spacing.
                    yPos += nextStyle.font.LineHeight * nextStyle.size + nextStyle.leading + nextStyle.paragraphSpacing;
                    continue;
                }

                float initialXPos;
                switch(nextStyle.alignment) {
                    case TextAlignment.Right:
                        initialXPos = rt.sizeDelta.x - GetStringWidth(line, nextStyle);
                        break;
                    case TextAlignment.Center:
                        initialXPos = (rt.sizeDelta.x - GetStringWidth(line, nextStyle)) / 2f;
                        break;
                    default:
                        initialXPos = 0f;
                        break;
                }

                BlitLine(line, initialXPos, yPos, nextStyle, vertsAndUvs);

                yPos += nextStyle.font.LineHeight * nextStyle.size + nextStyle.leading;
            }

            var verts = vertsAndUvs.Item1;
            mesh.SetVertices(verts);
            mesh.SetUVs(0, vertsAndUvs.Item2);

            var tris = new int[verts.Count / 4 * 6];
            for(int i = 0; i < verts.Count; i+=4) {
                int j = i / 4 * 6;
                tris[j  ] = i;
                tris[j+1] = i + 1;
                tris[j+2] = i + 2;
                tris[j+3] = i;
                tris[j+4] = i + 2;
                tris[j+5] = i + 3;
            }
            mesh.SetTriangles(tris, 0);

            mr.sharedMaterial = nextStyle.fontMaterial;
        }

        static void BlitLine(string line, float initialXPos, float yPos, TextStyle style, VertsAndUvs vertsAndUvs)
        {
            float xPos = initialXPos;
            TGlyph prevGlyph = null;
            foreach(char c in line) {

                var glyph = style.font.Glyphs.Get(c);

                if(glyph == null) {
                    continue;
                }

                if(c == ' ') {
                    xPos += glyph.xAdvance * style.size + style.tracking;
                    continue;
                }

                var kerning = GetKerning(prevGlyph, c, style.size);

                var localSpaceQuad = new Rect(
                    xPos + glyph.xOffset * style.size + kerning,
                    yPos + glyph.yOffset * style.size,
                    glyph.rect.width * style.size,
                    glyph.rect.height * style.size);


                BlitQuad(localSpaceQuad, glyph.rect, style.font.HScale, style.font.VScale, vertsAndUvs);

                xPos += glyph.xAdvance * style.size + style.tracking + kerning;
                prevGlyph = glyph;
            }
        }

        static void BlitQuad(Rect localSpaceQuad, Rect uvQuad, float hScale, float vScale, VertsAndUvs vertsAndUvs)
        {
            var verts = vertsAndUvs.Item1;
            var uvs = vertsAndUvs.Item2;

            verts.Add(new Vector3(localSpaceQuad.x, -localSpaceQuad.y, 0f));
            verts.Add(new Vector3(localSpaceQuad.x + localSpaceQuad.width, -localSpaceQuad.y, 0f));
            verts.Add(new Vector3(localSpaceQuad.x + localSpaceQuad.width, -localSpaceQuad.y - localSpaceQuad.height, 0f));
            verts.Add(new Vector3(localSpaceQuad.x, -localSpaceQuad.y - localSpaceQuad.height, 0f));

            uvs.Add(new Vector2(uvQuad.x / hScale, 1 - uvQuad.y / vScale));
            uvs.Add(new Vector2((uvQuad.x + uvQuad.width) / hScale, 1 - uvQuad.y / vScale));
            uvs.Add(new Vector2((uvQuad.x + uvQuad.width) / hScale, 1 - (uvQuad.y + uvQuad.height) / vScale));
            uvs.Add(new Vector2(uvQuad.x / hScale, 1 - (uvQuad.y + uvQuad.height) / vScale));
        }

        static IEnumerable<string> GetWrappedText(string text, TextStyle style, float width)
        {
            return text.Replace("\r\n", "\n").Split('\n').SelectMany(line => GetWrappedLines(line, style, width));
        }

        ///Takes a single line and returns one or more wrapped lines.
        static List<string> GetWrappedLines(string line, TextStyle style, float width)
        {
            var words = line.Split(' ');

            var newLines = new List<string>();

            int startOffset = 0;
            float cursorX = 0;

            for(int i = 0; i < words.Length; i++) {
                string adjustedWord;
                if(style.alignment == TextAlignment.Right) {
                    adjustedWord = " " + words[i];
                }
                else {
                    adjustedWord = words[i] + " ";
                }
                float wordWidth = GetStringWidth(adjustedWord, style);

                cursorX += wordWidth;

                if(cursorX > width) {
                    //The next line should start counting from the end of the wrapped word.
                    cursorX = wordWidth;
                    //Take all the words up to but not including the current word, and add them as a new line.
                    newLines.Add(string.Join(" ", words.Skip(startOffset).Take(i - startOffset).ToArray()));
                    startOffset = i;
                }
            }
            //Now take any remaining words.
            newLines.Add(string.Join(" ", words.Skip(startOffset).ToArray()));

            return newLines;
        }

        static float GetStringWidth(string s, TextStyle style)
        {
            TGlyph prevGlyph = null;
            float width = 0f;

            foreach(char c in s) {
                var glyph = style.font.Glyphs.Get(c);
                if(glyph == null) {
                    continue;
                }
                var kerning = GetKerning(prevGlyph, c, style.size);
                width += glyph.xAdvance * style.size + style.tracking + kerning;
                prevGlyph = glyph;
            }

            return width;
        }

        static float GetKerning(TGlyph prevGlyph, char c, float size)
        {
            return prevGlyph == null ? 0f : prevGlyph.GetKerning(c) * size;
        }
    }
}
