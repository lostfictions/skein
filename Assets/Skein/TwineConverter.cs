using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine.Assertions;

using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using System.Web;
using System.Xml.Linq;
using JetBrains.Annotations;
using UniRx;

namespace Skein
{
    public static class TwineTool
    {
        public static readonly string seersTwineFilePath = Path.Combine(Application.streamingAssetsPath, "seers.twine.xml");
        public static readonly ConverterRule[] seersExtraRules = {
            new ConverterRule {
                Name = "Parse <img onload> Scripts",
                Regex = new Regex(@"<img src=""data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="" style=""display: none;"" onload=""(?<Onload>[\s\S]*?)"">"),
                Replacer = match => "",
                ResultGenerator = match => {
                    //Debug.Log("To parse: " + match.Groups["Onload"].Value);
                    return RuleResult.Empty;
                }
            }
        };

        static readonly ConverterRule[] twineRules = {
            new ConverterRule {
                Name = "Links: [[Text->Link]] -> [Link|Text]",
                Regex = new Regex(@"\[\[(?<Text>.+?)->(?<Link>.+?)\]\]"),
                Replacer = match => "[" + match.Groups["Link"].Value + "|" + match.Groups["Text"].Value + "]",
                ResultGenerator = match => new RuleResult {
                    events = new [] { MakeOnClickGoto(match.Groups["Link"].Value) },
                    styles = new Tuple<string, TextStyle>[0]
                }
            },
            new ConverterRule {
                Name = "Links: [[Link<-Text]] -> [Link|Text]",
                Regex = new Regex(@"\[\[(?<Link>.+?)<-(?<Text>.+?)\]\]"),
                Replacer = match => "[" + match.Groups["Link"].Value + "|" + match.Groups["Text"].Value + "]",
                ResultGenerator = match => new RuleResult {
                    events = new [] { MakeOnClickGoto(match.Groups["Link"].Value) },
                    styles = new Tuple<string, TextStyle>[0]
                }
            },
            new ConverterRule {
                Name = "Links: [[Link]] -> [Link]",
                Regex = new Regex(@"\[\[(?<Link>.+?)\]\]"),
                Replacer = match => "[" + match.Groups["Link"].Value + "]",
                ResultGenerator = match => new RuleResult {
                    events = new [] { MakeOnClickGoto(match.Groups["Link"].Value) },
                    styles = new Tuple<string, TextStyle>[0]
                }
            },
            new ConverterRule {
                Name = "Show on Click: (click: ?Tag)[Contents]",
                Regex = new Regex(@"\(click:\s*\?(?<Tag>\w+?)\)\[(?<Contents>(?>[^\[\]]+|\[(?<DEPTH>)|\](?<-DEPTH>))+(?(DEPTH)(?!)))\]"),
                Replacer = match => "[OnClick_" + match.Groups["Tag"].Value + "|" + match.Groups["Contents"].Value + "]",
                ResultGenerator = match => new RuleResult {
                    events = new [] { MakeOnClickLog(match.Groups["Tag"].Value, "Gonna show the group \"OnClick_" + match.Groups["Tag"].Value + "\"!") },
                    styles = new Tuple<string, TextStyle>[0]
                }
            },
            new ConverterRule {
                Name = "Show on Click: (link: \"Text\")[Contents]",
                Regex = new Regex(@"\(link:\s*\?(?<LinkText>\w+?)\)\[(?<Contents>(?>[^\[\]]+|\[(?<DEPTH>)|\](?<-DEPTH>))+(?(DEPTH)(?!)))\]"),
                Replacer = match => {
//                    var autoLink = GetAutoLink(match.ToString());
//                    return "[" + autoLink + "|" + match.Groups["LinkText"].Value + "][OnClick_" + autoLink + "|" + match.Groups["Contents"].Value + "]";
                    return "";
                },
//                ResultGenerator = match => {
//                    var autoLink = GetAutoLink(match.ToString());
//                    return new RuleResult {
//                        events = new [] { MakeOnClickLog(autoLink, "Gonna show the group \"OnClick_" + autoLink + "\"!") },
//                        styles = new Tuple<string, TextStyle>[0]
//                    };
//                }
            },
            new ConverterRule {
                Name = "Hooks: [Text]<Tag|",
                Regex = new Regex(@"\[(?<Text>[^\[]+?)\]\<(?<Tag>\w+?)\|"),
                Replacer = match => "[" + match.Groups["Tag"].Value + "|" + match.Groups["Text"].Value + "]",
                ResultGenerator = match => RuleResult.Empty
            }
        };


        static long autoLinkCounter;
        static readonly Dictionary<string, long> autoLinks = new Dictionary<string, long>();
        static string GetAutoLink(string hash)
        {
            long l;
            if(!autoLinks.TryGetValue(hash, out l)) {
                autoLinkCounter += 1;
                l = autoLinkCounter;
                autoLinks.Add(hash, l);
            }
            return "AutoLink" + l;
        }

        public static IEnumerable<Passage> TwineToSkein(string twineFilePath, IEnumerable<ConverterRule> additionalConverters = null)
        {
            var converters = additionalConverters != null ?
                twineRules.Concat(additionalConverters) :
                twineRules;

            XElement root = XElement.Load(twineFilePath);

            return root
                .Descendants("tw-passagedata")
                .Select(n => {
                    var xy = n.Attribute("position").Value.Split(',').Select(s => int.Parse(s)).ToArray();
                    var pos = new Vector2(xy[0], -xy[1]) * 0.7f;

                    // It would be nicer to do this with a fold, but we want to be able to use
                    // Regex.Replace to get our matches so we don't have to do the text replacement
                    // ourselves. Hopefully this is reasonably understandable!
                    var ruleResults = new List<RuleResult>();
                    string txt = n.Value;
                    if(!string.IsNullOrEmpty(txt)) {
                        txt = HttpUtility.HtmlDecode(txt);
                        foreach(var converter in converters) {
                            var c = converter; //worst compiler
                            txt = converter.Regex.Replace(txt, match => {
                                ruleResults.Add(c.ResultGenerator(match));
                                return c.Replacer(match);
                            });
                        }
                    }

                    var p = new Passage(n.Attribute("name").Value, pos);
                    foreach(var style in ruleResults.SelectMany(r => r.styles)) {
                        p.StyleMap.Add(style.Item1, style.Item2);
                    }
                    foreach(var ev in ruleResults.SelectMany(r => r.events)) {
                        p.EventSheet.Add(ev);
                    }
                    p.Text.Value = txt;

                    return p;
                });
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Skein/Test Twine")]
        public static void TestPrint()
        {
            XElement root = XElement.Load(seersTwineFilePath);

            Debug.Log(root.Name);

            var names = root.Descendants("tw-passagedata")
                .Select(n => n.Attribute("name").Value)
                .Log(", ");

            Debug.Log(names);
        }

        public class ConverterRule
        {
            public string Name { [UsedImplicitly]get; set; }
            public Regex Regex { get; set; }
            public MatchEvaluator Replacer { get; set; }
            public Func<Match, RuleResult> ResultGenerator { get; set; }
        }

        public class RuleResult
        {
            public Event[] events;
            public Tuple<string, TextStyle>[] styles;

            public static RuleResult Empty
            {
                get { return new RuleResult { events = new Event[0], styles = new Tuple<string, TextStyle>[0] }; }
            }
        }

        #region Helpers

        //Some famous conditions and actions.
        static readonly Condition GroupWasClicked = WeaverInfo.Get(typeof(PassageDisplayText)).GetAll<Condition>().First(c => c.Name == "Group is clicked");
        static readonly Action Goto = WeaverInfo.Get(typeof(Story)).GetAll<Action>().First(c => c.Name == "Goto Passage");
        static readonly Action Log = WeaverInfo.Get(typeof(Story)).GetAll<Action>().First(c => c.Name == "Log");

        //Some somewhat well-regarded scene objects.
        static readonly Story Story = Object.FindObjectOfType<Story>();
        static readonly PassageDisplayText PassageDisplayText = Object.FindObjectOfType<PassageDisplayText>();

        static readonly ConditionData onClickPrototype = new ConditionData {
            info = GroupWasClicked,
            caller = PassageDisplayText,
            args = new object[0]
        };

        static readonly ActionData gotoPrototype = new ActionData {
            info = Goto,
            caller = Story,
            args = new object[0]
        };

        static readonly ActionData logPrototype = new ActionData {
            info = Log,
            caller = Story,
            args = new object[0]
        };

        static Event MakeOnClickGoto(string passageName)
        {
            var c = onClickPrototype.Clone<ConditionData, Condition>();
            c.args = new object[] { passageName };
            var a = gotoPrototype.Clone<ActionData, Action>();
            a.args = new object[] { passageName };
            return new Event {
                conditions = new ReactiveCollection<ConditionData> { c },
                actions = new ReactiveCollection<ActionData> { a }
            };
        }

        static Event MakeOnClickLog(string groupName, string message)
        {
            var c = onClickPrototype.Clone<ConditionData, Condition>();
            c.args = new object[] { groupName };
            var a = logPrototype.Clone<ActionData, Action>();
            a.args = new object[] { message };
            return new Event {
                conditions = new ReactiveCollection<ConditionData> { c },
                actions = new ReactiveCollection<ActionData> { a }
            };
        }

        #endregion

#endif
    }
}
