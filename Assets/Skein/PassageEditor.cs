using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using UnityEngine.UI;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;

namespace Skein
{
    public class PassageEditor : MonoBehaviour
    {
        #region Constants

        const string NEW_PASSAGE_NAME = "New Passage";
        const float CONTAINER_GROW_PADDING = 10f;
        const float PASSAGE_MODAL_PADDING = 20f;

        const string SPINNER_PATH = "Spinner";
        const string EVENT_SHEET_GROUP_PATH = SPINNER_PATH + "/" + "EventSheetGroup";
        const string TEXT_EDITING_GROUP_PATH = SPINNER_PATH + "/" + "TextEditingGroup";

        const string TITLE_INPUT_FIELD_PATH = TEXT_EDITING_GROUP_PATH + "/" + "Title";
        const string TEXT_INPUT_FIELD_PATH = TEXT_EDITING_GROUP_PATH + "/" + "Text";
        const string EVENT_SHEET_CONTENTS_PATH = EVENT_SHEET_GROUP_PATH + "/" + "ScrollRect/Viewport/Contents";
        const float FLIP_TIME = 0.4f;

        static readonly Color CURRENT_PASSAGE_HIGHLIGHT_COLOR = new Color(0, 1f, 1f, 1f);

        #endregion

        #region Inspector-assigned fields

        [Header("Prefabs")]
        public RectTransform passagePrefab;

        public Selectable modalBlockerPrefab;

        [Space]
        public RectTransform eventSheetContainerPrefab;
        public RectTransform eventSheetItemPrefab;

        public RectTransform conditionItemPrefab;
        public RectTransform actionItemPrefab;

        [Space]
        public RectTransform aceAdderModalPrefab;
        public RectTransform aceAdderModalHeaderPrefab;
        public RectTransform aceAdderModalItemPrefab;

        public RectTransform aceParameterPromptModalPrefab;

        public ParameterPrompts parameterPrompts;

        [Space]

        [Header("Scene Targets")]
        public RectTransform mainContainer;

        public RectTransform topBar;

        #endregion


        readonly Dictionary<Passage, RectTransform> passagesToTransforms = new Dictionary<Passage, RectTransform>();

        //TODO: maybe support multiple Stories?
        Story currentStory;
        public Story CurrentStory
        {
            get
            {
                if(currentStory == null) {
                    currentStory = FindObjectOfType<Story>();
                    if(currentStory == null) {
                        Debug.LogError("No Story objects found in scene!");
                    }
                }
                return currentStory;
            }
            set { currentStory = value; }
        }

        void Start()
        {
            var ps = CurrentStory.passages;

            ps.ObserveAdd()
                .StartWith(ps.Select((existingPassage, i) => new CollectionAddEvent<Passage>(i, existingPassage)))
                .Subscribe(added => {
                    var t = BindPassageToScene(added.Value);
                    passagesToTransforms.Add(added.Value, t);
                })
                .AddTo(this);

            ps.ObserveRemove().Subscribe(removed => {
                Destroy(passagesToTransforms[removed.Value].gameObject);
                passagesToTransforms.Remove(removed.Value);
                removed.Value.Dispose();
            }).AddTo(this);


            //Resize container to always be at least as large as the canvas.
            var canvasRt = mainContainer.GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            canvasRt.OnRectTransformDimensionsChangeAsObservable()
                .StartWith(new Unit())
                .Subscribe(_ => {
                    if(mainContainer.sizeDelta.x < canvasRt.sizeDelta.x) {
                        mainContainer.sizeDelta = new Vector2(canvasRt.sizeDelta.x, mainContainer.sizeDelta.y);
                    }
                    if(mainContainer.sizeDelta.y < canvasRt.sizeDelta.y) {
                        mainContainer.sizeDelta = new Vector2(mainContainer.sizeDelta.x, canvasRt.sizeDelta.y);
                    }
                });

            //Set highlight effect on currently selected passage.
            var es = EventSystem.current;
            Observable.EveryUpdate()
                .Select(_ => es.currentSelectedGameObject)
                .Buffer(2, 1)
                .Subscribe(selectables => {
                    if(selectables[0] != null && selectables[0].transform.IsChildOf(mainContainer)) {
                        var oldOl = selectables[0].GetComponent<Outline>();
                        if(oldOl != null) {
                            oldOl.enabled = false;
                        }
                    }
                    if(selectables[1] != null && selectables[1].transform.IsChildOf(mainContainer)) {
                        var newOl = selectables[1].GetComponent<Outline>();
                        if(newOl != null) {
                            newOl.enabled = true;
                        }
                    }
                });

            Outline currentPassageHighlight = null;
            CurrentStory.currentPassage
                .Select(cp => {
                    Passage p;
                    CurrentStory.PassagesByTitle.TryGetValue(cp, out p);
                    RectTransform pt = null;
                    if(p != null) {
                        passagesToTransforms.TryGetValue(p, out pt);
                    }
                    return pt;
                })
                .Subscribe(pt => {
                    if(currentPassageHighlight != null) {
                        if(currentPassageHighlight.GetComponent<RectTransform>() == pt) {
                            return;
                        }
                        Destroy(currentPassageHighlight);
                    }
                    if(pt != null) {
                        currentPassageHighlight = pt.gameObject.AddComponent<Outline>();
                        currentPassageHighlight.effectColor = CURRENT_PASSAGE_HIGHLIGHT_COLOR;
                        currentPassageHighlight.effectDistance = Vector2.one * 7f;
                        currentPassageHighlight.useGraphicAlpha = false;
                    }
                })
                .AddTo(this);


        }

        void Update()
        {
        	if(Input.GetKeyDown(KeyCode.BackQuote)) {
        		var c = GetComponent<Canvas>();
        		c.enabled = !c.enabled;
        	}
        }

        public void AddNewPassage()
        {
            Vector2 ap = mainContainer.anchoredPosition;
            float scale = mainContainer.localScale.x;

            // The passage name should be NEW_PASSAGE_NAME, unless there already exists a passage starting with that name.
            // In that case, we number them!
            string passageTitle = NEW_PASSAGE_NAME;
            var conflictingPassageNames = CurrentStory.passages
                .Select(p => p.Title.Value)
                .Where(title => title.StartsWith(NEW_PASSAGE_NAME));
            if(conflictingPassageNames.Any()) {
                var highest = conflictingPassageNames
                    .Select(title => title.Substring(NEW_PASSAGE_NAME.Length).Trim())
                    .Select(numString => {
                        int i;
                        return int.TryParse(numString, out i) ? new int?(i) : null;
                    })
                    .Max();
                if(highest.HasValue) {
                    passageTitle = NEW_PASSAGE_NAME + " " + (highest.Value + 1);
                }
                else {
                    passageTitle = NEW_PASSAGE_NAME + " 2";
                }
            }


            var newPassage = new Passage(passageTitle, new Vector2(-ap.x / scale, -ap.y / scale));

            CurrentStory.passages.Add(newPassage);
        }

        RectTransform BindPassageToScene(Passage p)
        {
            var passageTransform = Instantiate(passagePrefab);
            passageTransform.SetParent(mainContainer, false);

            var passageButton = passageTransform.GetComponent<Button>();

            passageButton
                .OnDoubleClickOrSubmitAsObservable()
                .Subscribe(_ => EnterPassageEditMode(passageTransform))
                .AddTo(passageButton);

            //Bring passage transform to front when it's selected.
            passageButton.OnSelectAsObservable()
                .Subscribe(_ => passageTransform.SetAsLastSibling())
                .AddTo(passageButton);


            SetupEventSheet(p, passageTransform);


            var titleField = passageTransform.FindChild(TITLE_INPUT_FIELD_PATH).GetComponent<InputField>();

            titleField.onEndEdit.AsObservable()
                .Subscribe(newTitle => {
                    if(newTitle != p.Title.Value) {
                        var conflictingPassage = passagesToTransforms.Keys.FirstOrDefault(s => s.Title.Value == newTitle);
                        if(conflictingPassage != null) {
                            //TODO: convert to in-editor warning
                            Debug.LogWarning("Can't set passage name to '" + newTitle + "': a passage with this name already exists!\nLocated at " + conflictingPassage.Position);
                            titleField.text = p.Title.Value;
                        }
                        else {
                            p.Title.Value = newTitle;
                        }
                    }
                })
                .AddTo(titleField);

            p.Title.Subscribe(newName => {
                    passageTransform.name = newName;
                    titleField.text = newName;
                })
                .AddTo(titleField);



            var textField = passageTransform.FindChild(TEXT_INPUT_FIELD_PATH).GetComponent<InputField>();

            var textFieldChanges = textField.onValueChange.AsObservable();

            textFieldChanges.Subscribe(newText => {
                    p.Text.Value = newText;
                })
                .AddTo(textField);

            //        textFieldChanges
            //            .Throttle(TimeSpan.FromSeconds(1))
            //            .DistinctUntilChanged()
            //            .Subscribe(newText => {
            //                debugUndoStack.Push(Tuple.Create(textField, newText));
            //                Debug.Log(p.Title.Value + " pushed new undo state: " + newText);
            //            })
            //            .AddTo(textField);



            p.Text.Subscribe(newText => {
                textField.text = newText;
            }).AddTo(textField);

            //        var sequenceDeleteButton = namePanelTransform.Find("DeleteButton").GetComponent<Button>();
            //        sequenceDeleteButton.onClick.AsObservable().Subscribe(_ => DeleteSequence(seq));

            var dragHandler = passageTransform.GetComponentInChildren<DragPanel>();
            dragHandler.PanelDragChanges.Subscribe(pos => p.Position.Value = pos).AddTo(dragHandler);
            p.Position.Subscribe(newPosition => passageTransform.localPosition = newPosition).AddTo(passageTransform);


            var deleteSelectedStream = Observable
                .EveryUpdate()
                .Where(_ => EventSystem.current != null &&
                    EventSystem.current.currentSelectedGameObject == passageTransform.gameObject &&
                    (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace))
                )
                .Take(1)
                .Select(_ => Unit.Default);

            var deleteButton = passageTransform.Find("DeleteButton").GetComponent<Button>();
            deleteButton.OnClickAsObservable()
                .Merge(deleteSelectedStream)
                .Take(1)
                .Subscribe(_ => CurrentStory.passages.Remove(p));


            p.Position.Subscribe(GrowContainerToFit).AddTo(passageTransform);
            
            // On reparse passage, set up arrows to targets.
            // This is a slightly gnarly way to do this, and in fact
            // we also want to re-check stuff when we add, delete or
            // rename a passage. so this remains a bit TODO
            var arrowUpdateSubs = new List<IDisposable>();
            p.ParsedText
                .Finally(() => {
                    foreach(var sub in arrowUpdateSubs) {
                        sub.Dispose();
                    }
                    arrowUpdateSubs.Clear();
                })
                .Subscribe(node => {
                    foreach(var sub in arrowUpdateSubs) {
                        sub.Dispose();
                    }
                    arrowUpdateSubs.Clear();
                    var names = Parser.GetNames(node);
                    foreach(var n in names) {
                        var targetPassage = passagesToTransforms.Keys.FirstOrDefault(p2 => p2.Title.Value == n);
                        if(targetPassage != null) {
                            var go = new GameObject(p.Title.Value + "->" + n);
                            var rt = go.AddOrGetComponent<RectTransform>();
                            rt.SetParent(mainContainer, false);
                            rt.SetAsFirstSibling();
                            rt.anchorMin = Vector2.zero;
                            rt.anchorMax = Vector2.zero;
                            rt.sizeDelta = Vector2.zero;
                            rt.localPosition = Vector2.zero;
                            rt.localScale = Vector3.one;
                            var arrow = go.AddComponent<UILineRenderer>();
                            var sub = p.Position
                                .CombineLatestCompleteOnEither(targetPassage.Position, (from, to) => new[] {from, to})
                                .Finally(() => Destroy(go))
                                .Subscribe(ft => {
                                    arrow.Points = ft;
                                    arrow.SetVerticesDirty();
                                })
                                .AddTo(passageTransform);
                            arrowUpdateSubs.Add(sub);
                        }
                    }
                }).AddTo(passageTransform);

            return passageTransform;
        }

        ///Instantiate event sheet, populate it, and bind it to the scene
        void SetupEventSheet(Passage p, RectTransform passageTransform)
        {
            var eventSheet = Instantiate(eventSheetContainerPrefab);

            //Cut out the "(Clone)" part of our instantiated name, since we're doing silly brittle name-based lookups.
            eventSheet.name = eventSheet.name.Substring(0, eventSheet.name.IndexOf("(Clone)", StringComparison.Ordinal));

            var spinnerTransform = passageTransform.Find(SPINNER_PATH);
            Assert.IsNotNull(spinnerTransform);
            eventSheet.SetParent(spinnerTransform, false);
            eventSheet.localEulerAngles = Vector3.up * 180f;
            eventSheet.anchoredPosition = Vector2.zero;
            eventSheet.sizeDelta = Vector2.zero;
            eventSheet.gameObject.SetActive(false);

            var eventSheetContainer = passageTransform.FindChild(EVENT_SHEET_CONTENTS_PATH);

            var addEventButton = eventSheetContainer.Find("AddButton").GetComponent<Button>();
            addEventButton.OnClickAsObservable()
                .SelectMany(_ => ShowEventAdderModal<ConditionData, Condition>())
                .Where(newCondition => newCondition != null)
                .Subscribe(newCondition => p.EventSheet.Add(new Event { conditions = { newCondition } }))
                .AddTo(addEventButton);

            var eventsToUiItems = new Dictionary<Event, RectTransform>();
            p.EventSheet
                .ObserveAdd()
                .StartWith(p.EventSheet.Select((existingEvent, i) => new CollectionAddEvent<Event>(i, existingEvent)))
                .Subscribe(addedEventItem => {
                    var ev = addedEventItem.Value;

                    var eventSheetItemTransform = Instantiate(eventSheetItemPrefab);
                    eventSheetItemTransform.SetParent(eventSheetContainer, false);

                    eventsToUiItems.Add(ev, eventSheetItemTransform);

                    var conditionsContainer = eventSheetItemTransform.FindChild("Conditions");
                    var actionsContainer = eventSheetItemTransform.FindChild("Actions");

                    var conditionsToUiItems = new Dictionary<ConditionData, RectTransform>();
                    var actionsToUiItems = new Dictionary<ActionData, RectTransform>();

                    conditionsContainer.Find("AddButton")
                        .GetComponent<Button>()
                        .OnClickAsObservable()
                        .SelectMany(_ => ShowEventAdderModal<ConditionData, Condition>())
                        .Where(newCondition => newCondition != null)
                        .Subscribe(newCondition => ev.conditions.Add(newCondition))
                        .AddTo(eventSheetItemTransform);

                    actionsContainer.Find("AddButton")
                        .GetComponent<Button>()
                        .OnClickAsObservable()
                        .SelectMany(_ => ShowEventAdderModal<ActionData, Action>())
                        .Where(newAction => newAction != null)
                        .Subscribe(newAction => ev.actions.Add(newAction))
                        .AddTo(eventSheetItemTransform);


                    ev.conditions
                        .ObserveAdd()
                        .StartWith(ev.conditions.Select((existingCondition, i) => new CollectionAddEvent<ConditionData>(i, existingCondition)))
                        .Subscribe(addedCondition => {
                            var conditionTransform = Instantiate(conditionItemPrefab);
                            conditionTransform.SetParent(conditionsContainer, false);

                            conditionTransform.Find("DeleteButton").GetComponent<Button>()
                                .OnClickAsObservable()
                                .Subscribe(_ => ev.conditions.Remove(addedCondition.Value))
                                .AddTo(conditionTransform);

                            conditionsToUiItems.Add(addedCondition.Value, conditionTransform);

                            conditionTransform.FindChild("Text").GetComponent<Text>().text =
                                addedCondition.Value.info.Name + 
                                (addedCondition.Value.args.Length > 0
                                    ? ": " + addedCondition.Value.args.Log(", ")
                                    : "");
                        })
                        .AddTo(eventSheetItemTransform);

                    ev.conditions
                        .ObserveRemove()
                        .Subscribe(removedCondition => {
                            Destroy(conditionsToUiItems[removedCondition.Value].gameObject);
                            conditionsToUiItems.Remove(removedCondition.Value);
                        })
                        .AddTo(passageTransform);

                    ev.actions
                        .ObserveAdd()
                        .StartWith(ev.actions.Select((existingAction, i) => new CollectionAddEvent<ActionData>(i, existingAction)))
                        .Subscribe(addedAction => {
                            var actionTransform = Instantiate(actionItemPrefab);
                            actionTransform.SetParent(actionsContainer, false);

                            actionTransform.Find("DeleteButton").GetComponent<Button>()
                                .OnClickAsObservable()
                                .Subscribe(_ => ev.actions.Remove(addedAction.Value))
                                .AddTo(actionTransform);

                            actionsToUiItems.Add(addedAction.Value, actionTransform);

                            actionTransform.FindChild("Text").GetComponent<Text>().text =
                                addedAction.Value.info.Name +
                                (addedAction.Value.args.Length > 0
                                    ? ": " + addedAction.Value.args.Log(", ")
                                    : "");
                        })
                        .AddTo(eventSheetItemTransform);

                    ev.actions
                        .ObserveRemove()
                        .Subscribe(removedAction => {
                            Destroy(actionsToUiItems[removedAction.Value].gameObject);
                            actionsToUiItems.Remove(removedAction.Value);
                        })
                        .AddTo(passageTransform);
                })
                .AddTo(passageTransform);

            p.EventSheet
                .ObserveRemove()
                .Subscribe(removedEventItem => {
                    Destroy(eventsToUiItems[removedEventItem.Value].gameObject);
                    eventsToUiItems.Remove(removedEventItem.Value);
                    removedEventItem.Value.Dispose();
                })
                .AddTo(passageTransform);
        }

        void EnterPassageEditMode(RectTransform passageTransform)
        {
            passageTransform.SetAsLastSibling();

            //Instantiate modal, set it as next-to-last sibling, and position it.
            var modalBlocker = Instantiate(modalBlockerPrefab);
            var mbTransform = modalBlocker.GetComponent<RectTransform>();
            mbTransform.SetParent(mainContainer, false);
            mbTransform.SetSiblingIndex(mainContainer.childCount - 2);

            // The edit mode transition may place the passage/viewport outside the boundaries of
            // the container, so sizing the modal blocker to the container may not suffice.
            // One way to adjust, if we know the passage's position, is this:

            mbTransform.anchoredPosition = passageTransform.anchoredPosition - Vector2.one * PASSAGE_MODAL_PADDING;

            // Another way, since both the passage and the blocker have their pivot in the lower-left
            // and the passage thus always expands up and to the right, is to just add the canvas width and height
            // to the blocker sizeDelta. We don't need to know about the passage for this:
            //mbTransform.anchoredPosition = Vector2.zero;
            //mbTransform.sizeDelta = GetComponentInParent<Canvas>().GetComponent<RectTransform>().sizeDelta;


            //Block all interaction with other UI components.
            //We'll explicitly enable interaction with our specific canvas group after the animation.
            var mcCanvasGroup = mainContainer.GetComponent<CanvasGroup>();
            mcCanvasGroup.interactable = false;
            mcCanvasGroup.blocksRaycasts = false;


            //HACK: EventSystem.current seems to return null inside this method when it's called through a Submit event.
            var es = FindObjectOfType<EventSystem>();
            es.enabled = false;


            //Disable scrollbars in main container, which requires us to disable the scrollrect as well.
            var sr = mainContainer.GetComponentInParent<ScrollRect>();
            sr.enabled = false;
            sr.horizontalScrollbar.gameObject.SetActive(false);
            sr.verticalScrollbar.gameObject.SetActive(false);


            var ptStartSize = passageTransform.sizeDelta;
            var ptEndSize = mainContainer.GetComponentInParent<Canvas>().GetComponent<RectTransform>().sizeDelta -
                new Vector2(PASSAGE_MODAL_PADDING * 2, topBar.sizeDelta.y + PASSAGE_MODAL_PADDING * 2);

            var mcStartPos = mainContainer.anchoredPosition;
            var mcEndPos = -(passageTransform.anchoredPosition + ptEndSize / 2f);

            var mbImage = modalBlocker.GetComponent<Image>();
            var mbEndColor = mbImage.color;
            mbImage.color = Color.clear;


            //Animate the transition, then enable interaction.
            Waiters.Interpolate(0.15f, f => mainContainer.anchoredPosition = Vector2.Lerp(mcStartPos, mcEndPos, Easing.EaseOut(f, EaseType.Quad)))
                .ThenInterpolate(0.2f, f => {
                    passageTransform.sizeDelta = Vector2.Lerp(ptStartSize, ptEndSize, Easing.EaseOut(f, EaseType.Quad));
                    mbImage.color = Color.Lerp(Color.clear, mbEndColor, f);
                })
                .Then(() => {
                    es.enabled = true;
                    var textEditCanvasGroup = passageTransform.Find(TEXT_EDITING_GROUP_PATH).GetComponent<CanvasGroup>();
                    textEditCanvasGroup.interactable = true;
                    textEditCanvasGroup.blocksRaycasts = true;


                    //When we hit the button, toggle between Event view and the text editing view.
                    var toggleButton = topBar.GetComponentsInChildren<Button>()
                        .First(b => b.name == "ToggleEventView");

                    //Flip the panel on hitting the button OR on shift-tab. 
                    var toggleEventViewStream = toggleButton
                        .OnClickAsObservable()
                        .Merge(Observable.EveryUpdate()
                            .Where(_ => toggleButton.interactable &&
                                Input.GetKeyDown(KeyCode.Tab) &&
                                (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
                            .Select(_ => Unit.Default))
                        //yes, we're basically keeping state in the DOM here. FIXME.
                        .Subscribe(_ => FlipPanel(passageTransform, textEditCanvasGroup.interactable));


                    //HACK: if we hit escape to close the modal, we should also grab the text and reassign it, since Unity
                    //will interpret it as a cancel event and we don't have a good way to override that behaviour.
                    //See HACK(pt2) below.
                    var input = passageTransform.FindChild(TEXT_INPUT_FIELD_PATH).GetComponent<InputField>();
                    string lastText = input.text;
                    var inputStream = input.OnValueChangeAsObservable().Subscribe(t => { if(!input.wasCanceled) lastText = t; });



                    // We close the modal either when we hit escape or when we click the modal background.
                    modalBlocker.OnPointerDownAsObservable()
                        .Select(_ => Unit.Default)
                        .Merge(Observable.EveryUpdate().Where(_ => Input.GetKeyDown(KeyCode.Escape)).Take(1).Select(_ => Unit.Default))
                        .Take(1)
                        .Subscribe(_ => {
                            //To close the modal view, do the opposite of what we did to open it, in reverse order.
                            es.enabled = false;


                            //If we need to flip the edit view back, do so before performing any other transition
                            Waiter waiter;
                            if(!textEditCanvasGroup.interactable) {
                                FlipPanel(passageTransform, false);
                                waiter = Waiters.Wait(FLIP_TIME);
                            }
                            else {
                                waiter = Waiters.Wait(0);
                            }


                            waiter.ThenInterpolate(0.2f, f => {
                                passageTransform.sizeDelta = Vector2.Lerp(ptEndSize, ptStartSize, Easing.EaseOut(f, EaseType.Quad));
                                mbImage.color = Color.Lerp(mbEndColor, Color.clear, f);
                            })
                                .ThenInterpolate(0.15f, f => mainContainer.anchoredPosition = Vector2.Lerp(mcEndPos, mcStartPos, Easing.EaseOut(f, EaseType.Quad)))
                                .Then(() => {
                                    es.enabled = true;
                                    textEditCanvasGroup.interactable = false;
                                    textEditCanvasGroup.blocksRaycasts = false;

                                    sr.enabled = true;

                                    mcCanvasGroup.interactable = true;
                                    mcCanvasGroup.blocksRaycasts = true;

                                    mbImage.color = mbEndColor;
                                    Destroy(modalBlocker.gameObject);
                                });

                            toggleEventViewStream.Dispose();

                            //HACK(pt2)
                            if(input.text != lastText) {
                                input.text = lastText;
                            }
                            inputStream.Dispose();

                        });
                });
        }

        void FlipPanel(RectTransform passageTransform, bool flipForwards)
        {
            var toggleButton = topBar.GetComponentsInChildren<Button>()
                .First(b => b.name == "ToggleEventView");
            toggleButton.interactable = false;

            var spinner = passageTransform.Find(SPINNER_PATH);

            var textEditCanvasGroup = passageTransform.Find(TEXT_EDITING_GROUP_PATH).GetComponent<CanvasGroup>();
            var eventViewCanvasGroup = passageTransform.Find(EVENT_SHEET_GROUP_PATH).GetComponent<CanvasGroup>();

            textEditCanvasGroup.interactable = false;
            textEditCanvasGroup.blocksRaycasts = false;

            eventViewCanvasGroup.interactable = false;
            eventViewCanvasGroup.blocksRaycasts = false;

            if(flipForwards) {
                Waiters
                    .Interpolate(FLIP_TIME / 2f, f => spinner.localEulerAngles = new Vector3(0, Easing.EaseIn(f, EaseType.Quad) * 90f, 0))
                    .Then(() => {
                        textEditCanvasGroup.gameObject.SetActive(false);
                        eventViewCanvasGroup.gameObject.SetActive(true);
                    })
                    .ThenInterpolate(FLIP_TIME / 2f, f => spinner.localEulerAngles = new Vector3(0, 90f + Easing.EaseOut(f, EaseType.Quad) * 90f, 0))
                    .Then(() => {
                        eventViewCanvasGroup.interactable = true;
                        eventViewCanvasGroup.blocksRaycasts = true;

                        toggleButton.interactable = true;
                    });
            }
            else {
                Waiters
                    .Interpolate(FLIP_TIME / 2f, f => spinner.localEulerAngles = new Vector3(0, 90f + Easing.EaseOut(1f - f, EaseType.Quad) * 90f, 0))
                    .Then(() => {
                        eventViewCanvasGroup.gameObject.SetActive(false);
                        textEditCanvasGroup.gameObject.SetActive(true);
                    })
                    .ThenInterpolate(FLIP_TIME / 2f, f => spinner.localEulerAngles = new Vector3(0, Easing.EaseIn(1f - f, EaseType.Quad) * 90f, 0))
                    .Then(() => {
                        textEditCanvasGroup.interactable = true;
                        textEditCanvasGroup.blocksRaycasts = true;

                        toggleButton.interactable = true;
                    });
            }
        }

        IObservable<TData> ShowEventAdderModal<TData, TInfo>()
            where TData : AceData<TInfo>, new()
            where TInfo : IAceInfo
        {
            var canvasTransform = mainContainer.GetComponentInParent<Canvas>().GetComponent<RectTransform>();

            var aceAdder = Instantiate(aceAdderModalPrefab);
            aceAdder.SetParent(canvasTransform, false);
            aceAdder.SetAsLastSibling();
//            aceAdder.anchoredPosition = new Vector2(0, -mainContainer.sizeDelta.y);
            aceAdder.anchoredPosition = Vector2.zero;

            //Instantiate modal, set it as next-to-last sibling, and position it.
            var modalBlocker = Instantiate(modalBlockerPrefab);
            var mbTransform = modalBlocker.GetComponent<RectTransform>();
            mbTransform.SetParent(canvasTransform, false);
            mbTransform.SetSiblingIndex(canvasTransform.childCount - 2);

            mbTransform.anchoredPosition = Vector2.zero;
            mbTransform.sizeDelta = Vector2.zero;

            //Set the modal title
            aceAdder.Find("TopBar/Text").GetComponent<Text>().text = "New " + typeof(TInfo).Name;

            var aceAdderContainer = aceAdder.Find("ScrollRect/Viewport/Contents");
            Assert.IsNotNull(aceAdderContainer);

            var buttonClicks = new List<IObservable<TData>>();

            foreach(var obj in SkeinDb.GetObjects()) {
                var objectType = obj.GetType();

                var weavables = WeaverInfo.Get(objectType).GetAll<TInfo>();

                if(!weavables.Any()) {
                    continue;
                }

                var objHeader = Instantiate(aceAdderModalHeaderPrefab);
                objHeader.SetParent(aceAdderContainer, false);

                objHeader.GetComponentInChildren<Text>().text = obj.name + " (" + objectType.Name + ")";

                foreach(var info in weavables) {
                    var methodItem = Instantiate(aceAdderModalItemPrefab);
                    methodItem.SetParent(aceAdderContainer, false);

                    methodItem.GetComponentInChildren<Text>().text = info.Name;

                    var objobj = obj; //our compiler sucks
                    var infoinfo = info;

                    var button = methodItem.GetComponent<Button>();

                    var click = button
                        .OnDoubleClickOrSubmitAsObservable()
                        .ObserveOnMainThread()
                        .SelectMany(_ => ShowAceParameterPromptsModal(infoinfo))
                        .Where(args => args != null)
                        .Select(args => new TData { info = infoinfo, caller = objobj, args = args.ToArray() });

                    buttonClicks.Add(click);
                }
            }


            var modalCancel = modalBlocker.OnPointerDownAsObservable().Select(_ => default(TData));
            buttonClicks.Add(modalCancel);

            var buttonCancel = aceAdder.Find("BottomBar/CancelButton").GetComponent<Button>()
                .OnClickAsObservable().Select(_ => default(TData));
            buttonClicks.Add(buttonCancel);

            var modalResult = buttonClicks.Merge().Take(1);

            modalResult.Subscribe(_ => {
                    Destroy(aceAdder.gameObject);
                    Destroy(modalBlocker.gameObject);
                });

            return modalResult;
        }

        IObservable<IList<object>> ShowAceParameterPromptsModal(IAceInfo aceInfo)
        {
            if(aceInfo.Arguments.Length == 0) {
                return Observable.Return<IList<object>>(new List<object>());
            }

            var canvasTransform = mainContainer.GetComponentInParent<Canvas>().GetComponent<RectTransform>();

            var parameterPromptModal = Instantiate(aceParameterPromptModalPrefab);
            parameterPromptModal.SetParent(canvasTransform, false);
            parameterPromptModal.SetAsLastSibling();
//            parameterPromptModal.anchoredPosition = new Vector2(0, -mainContainer.sizeDelta.y);
            parameterPromptModal.anchoredPosition = Vector2.zero;

            //Instantiate modal, set it as next-to-last sibling, and position it.
            var modalBlocker = Instantiate(modalBlockerPrefab);
            var mbTransform = modalBlocker.GetComponent<RectTransform>();
            mbTransform.SetParent(canvasTransform, false);
            mbTransform.SetSiblingIndex(canvasTransform.childCount - 2);

            mbTransform.anchoredPosition = Vector2.zero;
            mbTransform.sizeDelta = Vector2.zero;

            modalBlocker.GetComponent<Image>().color = Color.clear;


            //Set the modal title
            parameterPromptModal.Find("TopBar/Text").GetComponent<Text>().text = "Set Parameters for '" + aceInfo.Name + "'";

            var promptContainer = parameterPromptModal.Find("ScrollRect/Viewport/Contents");
            Assert.IsNotNull(promptContainer);

            var promptStreams = aceInfo.Arguments.Select(argInfo => {
                var prompt = Instantiate(parameterPrompts.GetPrompt(argInfo.type));
                prompt.transform.SetParent(promptContainer, false);
                prompt.transform.Find("Name").GetComponent<Text>().text = argInfo.description;
                return ((IParameterPrompt)prompt).Observable;
            });

            var argsStream = promptStreams.Zip();


            var doneButton = parameterPromptModal.Find("BottomBar/DoneButton").GetComponent<Button>();

            var confirmEnabled = argsStream.Select(args => args.All(o => o != null));
            confirmEnabled.StartWith(false).SubscribeToInteractable(doneButton);

            var confirmStream = doneButton.OnClickAsObservable().SelectMany(_ => argsStream);

            var cancelStream = parameterPromptModal
                .Find("BottomBar/BackButton")
                .GetComponent<Button>()
                .OnClickAsObservable()
                .Select(_ => default(IList<object>));


            var modalResult = confirmStream.Merge(cancelStream);

            modalResult.Subscribe(_ => {
                Destroy(parameterPromptModal.gameObject);
                Destroy(modalBlocker.gameObject);
            });

            //Scroll to top, if needed.
            parameterPromptModal.Find("ScrollRect").GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);

            return modalResult;
        }

        ///Resize the content rect to fit the sequences, if needed.
        /*
         * TODO:
         *  - check all sequence positions -- we need to resize down if we're shrinking the graph.
         */
        void GrowContainerToFit(Vector2 newPosition)
        {
            Vector2 sd = mainContainer.sizeDelta;

            Vector2 sizeAdjustment = Vector2.zero;
            Vector2 positionAdjustment = Vector2.zero;
            bool growContainer = false;
            bool adjustChildren = false;

            float leftdx = newPosition.x - CONTAINER_GROW_PADDING;
            if(leftdx < 0) {
                sizeAdjustment.x += -leftdx;
                positionAdjustment.x += -leftdx / 2;
                growContainer = true;
                adjustChildren = true;
            }
            else {
                float rightdx = (newPosition.x + passagePrefab.sizeDelta.x) - (sd.x - CONTAINER_GROW_PADDING);
                if(rightdx > 0) {
                    sizeAdjustment.x += rightdx;
                    positionAdjustment.x -= rightdx / 2;
                    growContainer = true;
                }
            }

            float bottomdy = newPosition.y - CONTAINER_GROW_PADDING;
            if(bottomdy < 0) {
                sizeAdjustment.y += -bottomdy;
                positionAdjustment.y += -bottomdy / 2;
                growContainer = true;
                adjustChildren = true;
            }
            else {
                float topdy = (newPosition.y + passagePrefab.sizeDelta.y) - (sd.y - CONTAINER_GROW_PADDING);
                if(topdy > 0) {
                    sizeAdjustment.y += topdy;
                    positionAdjustment.y -= topdy / 2;
                    growContainer = true;
                }
            }

            if(growContainer) {
                mainContainer.sizeDelta += sizeAdjustment;
                mainContainer.anchoredPosition += positionAdjustment;
            }
            if(adjustChildren) {
                var toAdjust = CurrentStory.passages; //.Where(passage => passage != p);
                foreach(var passage in toAdjust) {
                    passage.Position.Value += sizeAdjustment;
                }
            }
        }
    }

    public static class ObservableExtensions
    {
        ///Returns a stream that emits when the button is double-clicked or submitted.
        /// 
        ///(Submit events are defined by the Standalone Input Module as an axis name, which
        ///is itself defined in the Input Manager. Submit maps to return or space by default.)
        public static IObservable<Unit> OnDoubleClickOrSubmitAsObservable(this Button b)
        {
            return b
                .OnClickAsObservable()
                .Timestamp()
                .Buffer(2, 1)
                .Where(ts => ts[1].Timestamp - ts[0].Timestamp < TimeSpan.FromMilliseconds(250))
                .Select(_ => new Unit())
                .Merge(b.OnSubmitAsObservable().Select(_ => new Unit()));
        }
    }
}
