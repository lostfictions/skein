using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// So, about this class:
//
// While we'd like to have all of the Skein tools work entirely at runtime --
// and hence in a standalone build -- the reality is that we're working inside
// a Unity scene, within the Unity editor. This brings us a lot of benefits --
// we can integrate nicely with the regular Unity workflow, use other editor-
// only tooling, move cubes around with fancy drag handles, etc.
//
// Unfortunately, this also means we have to interoperate with Unity's own
// serialization and scene format. Skein conditions and actions can reference
// objects in the scene, and we need a way to serialize those references (in
// our own runtime serialization, not Unity's) and make sure they don't break
// later. Unity doesn't provide any runtime methods for this;
// Object.GetInstanceId() isn't robust to objects being moved around or even
// to editor restarts.
//
// So here's a class that lives in the scene and hooks into Unity's
// serialization system, designed to be updated when saving or entering
// playmode. (The editor class Skein.DbUpdater takes care of that.) Any types
// deriving from UnityEngine.Object with Skein-able methods (ie. those flagged
// as [Action] or [Condition]) are assigned a UID and will get saved here.
//
// There's nothing to prevent you from adding another of these to the scene
// (or worse, accidentally deleting it) and that sucks, but maybe we can work
// on that. Consider it TODO
//
// As a side note, one nice benefit of this approach is that we CAN still use
// the Skein tools at runtime, exclusive of being able to modify the
// serialized Unity scene. There's nothing stopping us from making a build and
// then inside that build changing events around or editing Story text and
// making that persist.
//

public class SkeinDb : MonoBehaviour
{
    [SerializeField, HideInInspector]
    List<string> ids = new List<string>();
    [SerializeField, HideInInspector]
    List<Object> objects = new List<Object>();
    
    Dictionary<Object, string> objectsToIds;
    Dictionary<string, Object> idsToObjects;

    static SkeinDb instance;
    static SkeinDb Instance
    {
        get
        {
            if(instance == null) {
                instance = FindObjectOfType<SkeinDb>();
            }
            Assert.IsNotNull(instance);
            return instance;
        }
    }

    void Awake()
    {
        objectsToIds = new Dictionary<Object, string>(ids.Count);
        idsToObjects = new Dictionary<string, Object>(ids.Count);

        Assert.AreEqual(ids.Count, objects.Count);

        for(int i = 0; i < ids.Count; i++) {
            objectsToIds.Add(objects[i], ids[i]);
            idsToObjects.Add(ids[i], objects[i]);
        }
    }

    public static Object GetObject(string id)
    {   
        return Instance.idsToObjects[id];
    }

    public static string GetId(Object obj)
    {
        return Instance.objectsToIds[obj];
    }

    public static IEnumerable<Object> GetObjects()
    {
        return Instance.objects.AsReadOnly();
    } 

#if UNITY_EDITOR
    public void Rebuild(IEnumerable<Object> updatedObjects)
    {
        // This isn't tremendously fast, but it should be okay for now. We
        // could alternately keep the lists sorted and traverse them in
        // parallel to figure out what to add/remove, but it probably isn't
        // worth the bother right now.

        var u = updatedObjects.ToArray();

        var toRemove = objects.Except(u).ToArray();
        foreach(var o in toRemove) {
            int i = objects.IndexOf(o);
            objects.RemoveAt(i);
            ids.RemoveAt(i);
        }

        var toAdd = u.Except(objects).ToArray();
        foreach(var o in toAdd) {
            objects.Add(o);
            ids.Add(Guid.NewGuid().ToString());
        }

    }

    [UnityEditor.MenuItem("Skein/Dump SkeinDb")]
    static void DumpDb()
    {
        Debug.Log(Instance.objects.Select((o, i) => new { Object = o, Id = Instance.ids[i] }).Log("\n"));
    }
#endif
}
