using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

using UniRx;
using UnityEngine.EventSystems;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    readonly Subject<Vector2> panelDragSubject = new Subject<Vector2>();
    public IObservable<Vector2> PanelDragChanges
    {
        get { return panelDragSubject.AsObservable(); }
    }

	RectTransform rt;

	Vector2 originalLocalPointerPosition;
	Vector3 originalPanelLocalPosition;
	RectTransform parentRectTransform;

    void Awake()
    {
        rt = GetComponent<RectTransform>();
    }

    void OnDestroy()
    {
        if(panelDragSubject != null) {
            panelDragSubject.OnCompleted();
        }
    }

	public void OnPointerDown(PointerEventData data)
	{
        parentRectTransform = (RectTransform)rt.parent;

		originalPanelLocalPosition = rt.localPosition;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentRectTransform,
            data.position,
            data.pressEventCamera,
            out originalLocalPointerPosition
        );
	}
	
	public void OnDrag(PointerEventData data)
	{
		if(rt == null || parentRectTransform == null) {
			return;
		}
		
		Vector2 localPointerPosition;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, data.position, data.pressEventCamera, out localPointerPosition)) {
			Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
			rt.localPosition = originalPanelLocalPosition + offsetToOriginal;
		}
		
		ClampToWindow();

        panelDragSubject.OnNext(rt.localPosition);
	}
	
	void ClampToWindow()
	{
		Vector3 pos = rt.localPosition;
		
		Vector3 minPosition = parentRectTransform.rect.min - rt.rect.min;
		Vector3 maxPosition = parentRectTransform.rect.max - rt.rect.max;
		
		pos.x = Mathf.Clamp(rt.localPosition.x, minPosition.x, maxPosition.x);
		pos.y = Mathf.Clamp(rt.localPosition.y, minPosition.y, maxPosition.y);
		
		rt.localPosition = pos;
	}
}
