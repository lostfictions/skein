using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using UnityEngine.EventSystems;

public class HoverShowDeleteButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    const float WAIT_TIME = 0.5f;

    GameObject deleteButton;

    Waiter activeWaiter;

    void Awake()
    {
        deleteButton = transform.Find("DeleteButton").gameObject;
        deleteButton.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if(data.pointerEnter == gameObject) {
            activeWaiter = Waiters
                .Wait(WAIT_TIME, gameObject)
                .Then(() => deleteButton.SetActive(true));    
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(activeWaiter != null) {
            Destroy(activeWaiter);
        }
        deleteButton.SetActive(false);
    }
}
