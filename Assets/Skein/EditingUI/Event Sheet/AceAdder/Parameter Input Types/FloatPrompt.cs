using UnityEngine;
using System;
using System.Collections.Generic;
using Skein;
using UniRx;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FloatPrompt : MonoBehaviour, IParameterPrompt
{
    public IObservable<object> Observable { get; private set; }

    void Awake()
    {
        var inputFieldStream = transform
            .Find("InputField")
            .GetComponent<InputField>()
            .OnValueChangeAsObservable();

        Observable = inputFieldStream
        	.Select<string, float?>(value => {
        	    float parsed;
                if(float.TryParse(value, out parsed)) {
                    return parsed; 
                }
        	    return null;
        	})
            .Where(f => f.HasValue)
            // ReSharper disable once PossibleInvalidOperationException
            .Select(f => f.Value)
            .Cast<float, object>();

//        inputFieldStream
//            .Select(newValue => !string.IsNullOrEmpty(newValue))
//            .Subscribe(isValid => inputBg.color = isValid ? validColor : invalidColor);
    }
}
