using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Skein
{
    public class ParameterPrompts : ScriptableObject
    {
        public ParameterMapping[] parameterMappings = new ParameterMapping[0];

        Dictionary<Type, MonoBehaviour> parameterMap;

        void OnEnable()
        {
            parameterMap = parameterMappings.ToDictionary(pm => Type.GetType(pm.type, true), pm => pm.prompt);

            foreach(var kvp in parameterMap) {
                if(!(kvp.Value is IParameterPrompt)) {
                    Debug.LogError(kvp.Value + " doesn't implement IParameterPrompt!");
                }
            }
        }

        public MonoBehaviour GetPrompt(Type t)
        {
            return parameterMap[t];
        } 
    }

    [Serializable]
    public class ParameterMapping
    {
        public string type;
        public MonoBehaviour prompt;
    }

    public interface IParameterPrompt
    {
        IObservable<object> Observable { get; }
        Transform transform { get; }
    }
}

