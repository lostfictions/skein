using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityCommon.Assert;

namespace Skein
{
    public interface INode {}
    public class TextNode : INode
    {
        public string text;
    }

    public class GroupNode : INode
    {
        public string name;
        public readonly List<INode> children = new List<INode>();
    }

    //TODO: real error handling instead of throwing exceptions everywhere, non-exhaustively :/
    public static class Parser
    {
        static readonly Regex tokenizerPattern = new Regex(@"([\[|\]])");

        [Pure]
        public static INode Parse(string passage)
        {
            return ParseNamedGroup(
                new GroupNode { name = "root_" + Guid.NewGuid() },
                Tokenize(passage));
        }

        [Pure]
        static INode ParseNamedGroup(GroupNode currentGroup, Queue<string> tokens)
        {
            Assert.That(() => !string.IsNullOrEmpty(currentGroup.name) && currentGroup.children.Count == 0);

            while(tokens.Count > 0) {
                var t = tokens.Dequeue();
           
                switch(t) {
                    // if our token is text, just add the text.
                    default:
                        currentGroup.children.Add(new TextNode {text = t});
                        break;
                    // add a new group.
                    case "[":
                        currentGroup.children.Add(
                            ParseNameOrAnonymousGroup(
                                new GroupNode(),
                                tokens));
                        break;
                    // ignore the group name delimiter in already-named groups! it should be treated like text.
                    case "|":
                        if(currentGroup.children.Count == 0) {
                            currentGroup.children.Add(new TextNode { text = "|" });
                        }
                        else {
                            var lc = currentGroup.children.Last();
                            if(lc is TextNode) {
                                var tn = (TextNode)lc;
                                tn.text = tn.text + "|";
                            }
                            else {
                                currentGroup.children.Add(new TextNode { text = "|" });
                            }
                        }
                        break;
                    case "]":
                        return currentGroup;
                }
            }

            //FIXME: handle case of being inside named group when tokens run out.
            return currentGroup;
        }

        [Pure]
        static INode ParseNameOrAnonymousGroup(GroupNode currentGroup, Queue<string> tokens)
        {
            Assert.That(() => string.IsNullOrEmpty(currentGroup.name) && currentGroup.children.Count == 0);

            while(tokens.Count > 0) {
                var t = tokens.Dequeue();

                switch(t) {
                    //if our token is text...
                    default:
                        // if we might be parsing a name OR an anonymous group, look ahead to see which it is.
                        if(tokens.Count == 0) {
                            // TODO: we're inside a group but we've run out of tokens. probably a parse error since we're
                            // expecting a "]" at least, but let it slide for now and use the anonymous group case.
                            currentGroup.name = t;
                            currentGroup.children.Add(new TextNode {text = t});
                            return currentGroup;
                        }

                        var nt = tokens.Dequeue();

                        switch(nt) {
                            // if there's a "|", we're parsing a name. set it and then parse the contents.
                            case "|":
                                currentGroup.name = t;

                                return ParseNamedGroup(currentGroup, tokens);
                            // if there's a "]", we're parsing an anonymous group and these are the whole contents.
                            // generate a name and close the group.
                            case "]":
                                currentGroup.name = t;
                                currentGroup.children.Add(new TextNode {text = t});
                                return currentGroup;
                            // if there's a "[", we're parsing an anonymous group AND we have a nested group.
                            // generate a name and parse the contents.
                            case "[":
                                currentGroup.name = t;
                                currentGroup.children.Add(ParseNameOrAnonymousGroup(
                                    new GroupNode(),
                                    tokens));

                                return ParseNamedGroup(currentGroup, tokens);
                            default:
                                throw new Exception("This should never happen! Current token: " + t + "Next token: " + nt);
                        }
                    case "[":
                    case "|":
                    case "]":
                        //FIXME: we could land here if we get consecutive delimiter tokens.
                        //particularly possible for something like "[[hey] hello]"
                        throw new Exception("This should never happen! Next token: " + tokens.Peek());
                }
            }

            //FIXME: we could get here if the last character is a "[".
            throw new Exception("Should never arrive here!");
        }

        [Pure]
        static Queue<string> Tokenize(string passage)
        {
            return new Queue<string>(tokenizerPattern.Split(passage));
        }

        [Pure]
        public static IEnumerable<string> GetNames(INode node)
        {
            var hs = new HashSet<string>();
            GetNames(node, ref hs);
            return hs.Skip(1);
        }

        static void GetNames(INode node, ref HashSet<string> names)
        {
            if(node is TextNode) {
                return;
            }
            var gn = (GroupNode)node;
            names.Add(gn.name);
            foreach(var c in gn.children) {
                GetNames(c, ref names);
            }
        }
    }
}
