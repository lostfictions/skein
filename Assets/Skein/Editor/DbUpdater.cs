using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using UnityEditor;

namespace Skein
{
    [InitializeOnLoad]
    public class DbUpdater : UnityEditor.AssetModificationProcessor
    {
        static DbUpdater()
        {
            EditorApplication.playmodeStateChanged += () => {
                //If we're not in playmode yet but we're about to enter it, update the DB. Changes can still be serialized.
                if(!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode) {
                    UpdateObjectDb();
                }
            };
        }

        public static void OnWillSaveAssets(string[] assets)
        {
            string cs = EditorApplication.currentScene;

            if(assets.Any(path => path == cs)) {
                UpdateObjectDb();
            }
        }

        static void UpdateObjectDb()
        {
            var skeinDb = Object.FindObjectOfType<SkeinDb>();
            skeinDb.Rebuild(GetSkeinableObjects());
            EditorUtility.SetDirty(skeinDb);
        }

        static IEnumerable<Object> GetSkeinableObjects()
        {
            var skeinableTypes = Assembly.GetAssembly(typeof(Story))
                .GetTypes()
                .Where(type =>
                    typeof(Object).IsAssignableFrom(type) &&
                    type.GetMethods().Any(mi =>
                        Attribute.IsDefined(mi, typeof(ActionAttribute)) ||
                        Attribute.IsDefined(mi, typeof(ConditionAttribute))));

            return skeinableTypes.SelectMany(type => Object.FindObjectsOfType(type));
        }
    }
}
