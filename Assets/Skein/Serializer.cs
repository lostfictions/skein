using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using System.IO;
using System.Linq;
using System.Reflection;
using FullSerializer;
using UnityEngine.Assertions;

namespace Skein
{
    public static class Serializer
    {
        static Serializer()
        {
            serializer = new fsSerializer();
        }

        static fsSerializer serializer;

        public static void SerializePassages(string path, IEnumerable<Passage> passages)
        {
            fsData data;
            var result = serializer.TrySerialize(passages, out data);
            if(result.Failed) {
                throw new Exception(result.FormattedMessages);
            }

            //Debug.Log(fsJsonPrinter.PrettyJson(data));

#if !UNITY_WEBPLAYER
            File.WriteAllText(path, fsJsonPrinter.PrettyJson(data));
            Debug.Log("Serialized to " + path + "!");
#endif
#if UNITY_EDITOR && !UNITY_WEBPLAYER
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public static IEnumerable<Passage> DeserializePassages(string json)
        {
            if(json.Length == 0) {
                return Enumerable.Empty<Passage>();
            }

            fsData data;
            var result = fsJsonParser.Parse(json, out data);
            if(result.Failed) {
                throw new Exception(result.FormattedMessages);
            }

            List<Passage> passages = null;
            result = serializer.TryDeserialize(data, ref passages);
            if(result.Failed) {
                throw new Exception(result.FormattedMessages);
            }

            passages.Each(p => p.ConnectParsedText());

            return passages;
        }
    }

    #region Sacrifices to the JSON god
    public class ActionDataConverter : AceDataConverter<ActionData, Action> { }
    public class ConditionDataConverter : AceDataConverter<ConditionData, Condition> { }

    public abstract class AceDataConverter<TData, TInfo> : fsDirectConverter<TData>
        where TData : AceData<TInfo>, new()
        where TInfo : IAceInfo
    {
        protected override fsResult DoSerialize(TData dataToSerialize, Dictionary<string, fsData> serialized)
        {
            var result = fsResult.Success;
            result += SerializeMember(serialized, "methodId", dataToSerialize.info.MethodId);
            result += SerializeMember(serialized, "caller", SkeinDb.GetId(dataToSerialize.caller));

            //If an argument is a UnityEngine.Object, we serialize by getting a UID instead of trying to serialize the object proper.
            result += SerializeMember(serialized, "args", dataToSerialize.args.Select(arg => {
                var o = arg as Object;
                if(o != null) {
                    return SkeinDb.GetId(o);
                }
                return arg;
            }).ToArray());

            return result;
        }

        protected override fsResult DoDeserialize(Dictionary<string, fsData> data, ref TData model)
        {
            var result = fsResult.Success;

            string methodId;
            result += DeserializeMember(data, "methodId", out methodId);
            TInfo info = InfoStorage<TInfo>.GetFromMethodId(methodId);

            string callerUid;
            result += DeserializeMember(data, "caller", out callerUid);
            Object caller = SkeinDb.GetObject(callerUid);

            object[] args;
            result += DeserializeMember(data, "args", out args);

            Assert.AreEqual(info.Arguments.Length, args.Length);

            for(int i = 0; i < args.Length; i++) {
                if(typeof(Object).IsAssignableFrom(info.Arguments[i].type)) {
                    args[i] = SkeinDb.GetObject((string)args[i]);
                }
            }

            model.info = info;
            model.caller = caller;
            model.args = args;

            return result;
        }

        public override object CreateInstance(fsData data, Type storageType)
        {
            return new TData();
        }
    }
    #endregion
}
