using System;
using UnityEngine;
using System.Collections.Generic;

using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.Assertions;

namespace Skein
{
    public class Story : MonoBehaviour
    {
        const string DEFAULT_FILE_NAME = "teststory";

        #region Reactive Properties

        #region Persistent State

        public ReactiveCollection<Passage> passages;

        public TextStyleReactiveProperty defaultTextStyle;
        public TextStyleReactiveProperty defaultLinkStyle;

        #endregion

        #region "Transient"/Resettable State
        // TODO: these should each be split into two properties: one serialized prop which
        // represents the initial value, and one non-serialized prop representing the value
        // at runtime. That lets us easily reset to our initial state -- currently we have to
        // exit playmode to do so.

        /// TODO: This is a string so that we can mess around with it in the Editor.
        /// Eventually it should probably just be the Passage itself, no need to use the map.
        
        //FIXME: should never be allowed to be set to an invalid value!
        public StringReactiveProperty currentPassage;

        public PassageDisplayTextReactiveProperty targetText;

        #endregion

        #endregion

        ///A cache for more quickly fetching passages by their title.
        public Dictionary<string, Passage> PassagesByTitle { get; private set; }

//        bool isPassageBeingEntered;
        float passageTime;

	    void Start()
	    {
            DebugDeserialize();

            PassagesByTitle = new Dictionary<string, Passage>();

            passages.ObserveAdd()
                //Start with the already deserialized passages
                .StartWith(passages.Select((existingPassage, i) => new CollectionAddEvent<Passage>(i, existingPassage)))
                .Subscribe(added => PassagesByTitle[added.Value.Title.Value] = added.Value)
                .AddTo(this);

	        passages.ObserveRemove()
	            .Subscribe(removed => {
	                if(currentPassage.Value == removed.Value.Title.Value) {
                        Debug.LogWarning("Active passage was deleted!");
	                    currentPassage.Value = "";
	                }

	                var wasRemoved = PassagesByTitle.Remove(removed.Value.Title.Value);
                    Assert.IsTrue(wasRemoved);
	                removed.Value.Dispose();
	            })
                .AddTo(this);

            //TODO: simplify this logic
	        IDisposable rebuildSub = null;
	        currentPassage
                .CombineLatest(targetText, (passageName, passageDisplayText) => new { passageName, passageDisplayText })
                .Subscribe(next => {
                    var pn = next.passageName;
                    var tt = next.passageDisplayText;

                    if(tt == null) {
                        return;
                    }

                    Passage passage;
                    if(!PassagesByTitle.TryGetValue(pn, out passage)) {
                        Debug.LogWarning("Passage name not found in current Story: " + pn);
                    }
                    else {
                        if(rebuildSub != null) {
                            rebuildSub.Dispose();
                            rebuildSub = null;
                        }

                        rebuildSub = Observable.Merge(
                                passage.ParsedText.Select(_ => Unit.Default),
                                tt.OnRectTransformDimensionsChangeAsObservable().StartWith(Unit.Default),
                                defaultTextStyle.Select(_ => Unit.Default),
                                defaultLinkStyle.Select(_ => Unit.Default),
                                passage.PassageTextStyle.Select(_ => Unit.Default),
                                passage.PassageLinkStyle.Select(_ => Unit.Default))
                            .Subscribe(_ => tt.RebuildMesh(passage, defaultTextStyle.Value, defaultLinkStyle.Value))
                            .AddTo(this);
                    }
                })
                .AddTo(this);

	        currentPassage
	            .DistinctUntilChanged()
	            .Select(passageName => PassagesByTitle.ContainsKey(passageName))
	            .Subscribe(wasChanged => {
	                passageTime = 0f;
//	                isPassageBeingEntered = true;
	            });
	    }

        void Update()
        {
            Passage p;
            if(!PassagesByTitle.TryGetValue(currentPassage.Value, out p)) {
                return;
            }
            foreach(var ev in p.EventSheet) {
                //An event with no conditions will always fail.
                if(ev.conditions.Count == 0) {
                    continue;
                }
                if(ev.conditions.All(c => c.Test())) {
                    foreach(var action in ev.actions) {
                        action.Invoke();
                    }
                }
            }
        }

        void LateUpdate()
        {
//            isPassageBeingEntered = false;
            passageTime += Time.deltaTime;
        }

        public void DebugDeserialize()
        {
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(WeaverInfo).TypeHandle);

#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
            var json = new WWW(Application.streamingAssetsPath + '/' + DEFAULT_FILE_NAME + ".json");
#else
            var json = new WWW("file://" + Application.streamingAssetsPath + '/' + DEFAULT_FILE_NAME + ".json");
#endif
            while(!json.isDone) {}
            if(!string.IsNullOrEmpty(json.error)) {
                Debug.Log(json.error);
            }

            passages = new ReactiveCollection<Passage>(Serializer.DeserializePassages(json.text));
        }

        public void DebugSerialize()
        {
            var currentJsonFilename = Application.streamingAssetsPath + '/' + DEFAULT_FILE_NAME + ".json";
            Serializer.SerializePassages(currentJsonFilename, passages);
        }

        #region Actions, Conditions, Expressions

        #region Actions

        [Action("Goto Passage", "Jump to a passage by name", "Passage name to jump to")]
        public void Goto(string passageName)
        {
            currentPassage.Value = passageName;
        }

        [Action("Log", "Print a message to the Unity console", "Message to log")]
        public void Log(string message)
        {
            Debug.Log(message);
        }

        #endregion

        //TODO: make static/ownerless!
        #region Conditions

        [Condition("Always (Every Tick)", "Triggers on every tick")]
        public bool Always()
        {
            return true;
        }

//        [Condition("On Passage Enter", "Triggers when the passage is entered")]
//        public bool OnPassageEnter()
//        {
//            return isPassageBeingEntered;
//        }

        [Condition("After X Seconds",
            "Triggers after a given number of seconds have elapsed in the given passage",
            "Amount of time to wait")]
        public bool After(float time)
        {
            return passageTime > time;
        }

        #endregion

        #endregion
    }

    #region UniRx boilerplate
    [Serializable]
    public class PassageDisplayTextReactiveProperty : ReactiveProperty<PassageDisplayText> { }
#if UNITY_EDITOR
    [UnityEditor.CustomPropertyDrawer(typeof(PassageDisplayTextReactiveProperty))]
    // ReSharper disable RedundantExtendsListEntry
    public partial class ExtendInspectorDisplayDrawer : InspectorDisplayDrawer { }
    // ReSharper restore RedundantExtendsListEntry
#endif
    #endregion
}
