using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

//Based on http://answers.unity3d.com/answers/809221/view.html
[Serializable]
public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField]
    readonly List<TKey> _keys = new List<TKey>();

    [SerializeField]
    readonly List<TValue> _values = new List<TValue>();

    public void OnBeforeSerialize()
    {
        _keys.Clear();
        _values.Clear();

        if(typeof(TKey).IsSubclassOf(typeof(UnityEngine.Object)) || typeof(TKey) == typeof(UnityEngine.Object)) {
            foreach(var element in this.Where(element => element.Key != null)) {
                _keys.Add(element.Key);
                _values.Add(element.Value);
            }
        }
        else {
            foreach(var element in this) {
                _keys.Add(element.Key);
                _values.Add(element.Value);
            }
        }
    }

    public void OnAfterDeserialize()
    {
        Clear();

        if(_keys.Count != _values.Count) {
            throw new Exception(string.Format("There are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable.", _keys.Count, _values.Count));
        }

        for(int i = 0; i < _keys.Count; i++) {
            Add(_keys[i], _values[i]);
        }
    }
}
