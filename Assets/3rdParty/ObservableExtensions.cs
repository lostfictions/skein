using System;
using System.Collections.Generic;

namespace UniRx
{
    public static partial class Observable
    {
        public static IObservable<TResult> CombineLatestCompleteOnEither<TLeft, TRight, TResult>(this IObservable<TLeft> left, IObservable<TRight> right, Func<TLeft, TRight, TResult> selector)
        {
            return Observable.Create<TResult>(observer => {
                var gate = new object();

                var leftValue = default(TLeft);
                var leftStarted = false;

                var rightValue = default(TRight);
                var rightStarted = false;

                Action run = () => {
                    if(!(leftStarted && rightStarted)) {
                        return;
                    }

                    TResult v;
                    try {
                        v = selector(leftValue, rightValue);
                    }
                    catch(Exception ex) {
                        observer.OnError(ex);
                        return;
                    }
                    observer.OnNext(v);
                };

                var lsubscription = left.Synchronize(gate).Subscribe(x => {
                    leftStarted = true;
                    leftValue = x;
                    run();
                }, observer.OnError, observer.OnCompleted);

                var rsubscription = right.Synchronize(gate).Subscribe(x => {
                    rightStarted = true;
                    rightValue = x;
                    run();
                }, observer.OnError, observer.OnCompleted);

                return new CompositeDisposable { lsubscription, rsubscription };
            }, isRequiredSubscribeOnCurrentThread: left.IsRequiredSubscribeOnCurrentThread() && right.IsRequiredSubscribeOnCurrentThread());
        }
    }
}
