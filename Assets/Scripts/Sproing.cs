using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class Sproing : MonoBehaviour
{
    Waiter w;
    Vector3 initScale;

    [Skein.Action("Spoing", "Do some sproinging", "How much sproing", "How long sproing")]
    public void DoSproing(float amount, float duration)
    {
        if(w != null) {
            Destroy(w);
            transform.localScale = initScale;
        }
        initScale = transform.localScale;
        w = Waiters.Interpolate(duration, f => {
            transform.localScale = Vector3.Lerp(initScale * amount, initScale, Easing.EaseOut(f, EaseType.Elastic));
        });
    }
}
