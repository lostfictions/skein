using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Renderer))]
public class ColorChanger : MonoBehaviour
{
    Renderer ren;
    Renderer Renderer
    {
        get
        {
            if(ren == null) {
                ren = GetComponent<Renderer>();
            }
            return ren;
        }
    }

    [Skein.Action("Set color", "Change the color of an object's renderer to the specified color", "The color to set")]
    public void SetColor(Color newColor)
    {
        Renderer.material.color = newColor;
    }


    [Skein.Action("Set random color", "Change the color of an object's renderer to a random value")]
    public void SetRandomColor()
    {
        Renderer.material.color = new Color(Random.value, Random.value, Random.value);
    }
}
