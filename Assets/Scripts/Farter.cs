using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class Farter : MonoBehaviour
{
    AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        Assert.IsNotNull(source);
    }

    [Skein.Action("Fart", "Farrrrt")]
    public void Fart()
    {
        source.Play();
    }
}
